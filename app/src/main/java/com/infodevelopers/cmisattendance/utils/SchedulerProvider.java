package com.infodevelopers.cmisattendance.utils;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SchedulerProvider {

    private static SchedulerProvider INSTANCE;

    @Inject
    public SchedulerProvider() {
    }

    public static synchronized SchedulerProvider getINSTANCE(){
        if(INSTANCE==null){
            INSTANCE= new SchedulerProvider();
        }
        return INSTANCE;
    }
    public Scheduler computation(){
        return Schedulers.computation();
    }

    public Scheduler io(){
        return Schedulers.io();
    }

    public Scheduler ui(){
        return AndroidSchedulers.mainThread();
    }

}
