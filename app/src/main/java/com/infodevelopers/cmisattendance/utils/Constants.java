package com.infodevelopers.cmisattendance.utils;

public class Constants {
    public static final String SELECT = "SELECT";
    public static final int NUMBER_DOWNLOAD_SETUP = 4;
    public static final String VIEW_GONE = "VIEW_GONE";
    public static final String DATABASE_NAME ="user" ;

    public static String TAG_CHILD_DETAILS = "CHILD_DATA";

    public static String TAG_MASTER_ID = "TAG_MASTER_ID";
    public static String TAG_DATA = "TAG_DATA";
    public static String TAG_TYPE_ADULT = "TAG_ADULT";

    public static String TAG_TYPE_CHILD = "TAG_CHILD";

    public static String TAG_EDIT = "Edit";
    public static String TAG_DELETE = "Delete";
    public static String TAG_SAVE = "TAG_SAVE";
    public static String TAG_UPDATE = "TAG_UPDATE";
}
