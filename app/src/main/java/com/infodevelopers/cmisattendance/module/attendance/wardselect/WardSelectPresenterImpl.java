package com.infodevelopers.cmisattendance.module.attendance.wardselect;

import android.util.Log;

import com.google.gson.Gson;
import com.infodevelopers.cmisattendance.base.presenter.BasePresenter;
import com.infodevelopers.cmisattendance.data.local.model.ActivityModel;
import com.infodevelopers.cmisattendance.data.local.model.Attendance;
import com.infodevelopers.cmisattendance.data.local.model.Child;
import com.infodevelopers.cmisattendance.data.local.model.District;
import com.infodevelopers.cmisattendance.data.local.model.VDC;
import com.infodevelopers.cmisattendance.data.local.model.Ward;
import com.infodevelopers.cmisattendance.data.repository.DataRepository;
import com.infodevelopers.cmisattendance.module.attendance.pojo.DownloadedWard;
import com.infodevelopers.cmisattendance.utils.SchedulerProvider;

import org.apache.commons.lang.ArrayUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.CompletableObserver;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.subscribers.DisposableSubscriber;
import io.reactivex.subscribers.ResourceSubscriber;

public class WardSelectPresenterImpl<V extends WardSelectView> extends BasePresenter<V> implements WardSelectPresenter<V> {


    @Inject
    public WardSelectPresenterImpl(SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable, DataRepository dataRepository) {
        super(schedulerProvider, compositeDisposable, dataRepository);
    }

    @Override
    public void getDistrict() {
        getCompositeDisposable().add(getDataRepository().getAllDistrict()
                .observeOn(getSchedulerProvider().ui())
                .subscribeOn(getSchedulerProvider().io())
                .subscribe(new Consumer<List<District>>() {
                    @Override
                    public void accept(List<District> districtList) throws Exception {
                        getMvpView().showDistrict(districtList);
                    }}
                    , throwable -> {
                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();

                        // handle the error here
//                            if (throwable instanceof ANError) {
//                                ANError anError = (ANError) throwable;
//                                handleApiError(anError);
//                            }
                    }));

//                    @Override
//                    public void onNext(List<District> districtList) {
//                        getMvpView().showDistrict(districtList);
//                    }
//
//                    @Override
//                    public void onError(Throwable t) {
//
//                    }
//
//                    @Override
//                    public void onComplete() {
//
//                    }
//                }));
    }

    @Override
    public void getVdc(int district_id) {
getCompositeDisposable().add(getDataRepository().getVdcOfDistrict(district_id)
.observeOn(getSchedulerProvider().ui())
.subscribeOn(getSchedulerProvider().io())
.subscribeWith(new DisposableSubscriber<List<VDC>>() {
    @Override
    public void onNext(List<VDC> vdcList) {
        getMvpView().showVdc(vdcList);
    }

    @Override
    public void onError(Throwable t) {

    }

    @Override
    public void onComplete() {

    }
}));


    }

    @Override
    public void getWard(int vdc_id) {
       getCompositeDisposable().add(getDataRepository().getDownloadedWardInfo()
                .observeOn(getSchedulerProvider().io())
                .subscribe(new Consumer<List<Ward>>() {
                    @Override
                    public void accept(List<Ward> downloadedWards) throws Exception {
                   Log.d("download",new Gson().toJson(downloadedWards));
                    }
                }));






getCompositeDisposable().add(getDataRepository().getWardOfVdc(vdc_id)
        .observeOn(getSchedulerProvider().ui())
        .subscribeOn(getSchedulerProvider().io())
        .subscribeWith(new DisposableSubscriber<List<Ward>>() {
            @Override
            public void onNext(List<Ward> wardList) {
            getMvpView().showWard(wardList);
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        }));
    }

    @Override
    public void getChildData(String district_id, String vdc_id, String ward_id) {
        getCompositeDisposable().add(getDataRepository().getChildData(district_id,vdc_id,ward_id)
                .observeOn(getSchedulerProvider().ui())
                .subscribeOn(getSchedulerProvider().io())
                .subscribeWith(new DisposableSubscriber<List<Child>>() {
                    @Override
                    public void onNext(List<Child> childList) {
                        Log.d("child",ArrayUtils.toString(childList));
                        getMvpView().showChildData(childList);
                    }

                    @Override
                    public void onError(Throwable t) {

                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }


    @Override
    public void getAllActivity() {
        getCompositeDisposable().add(getDataRepository().getAllActivity().
                observeOn(getSchedulerProvider().ui())
                .subscribeOn(getSchedulerProvider().io())
                .subscribeWith(new DisposableSubscriber<List<ActivityModel>>(){
                    @Override
                    public void onNext(List<ActivityModel> activityModels) {
                        getMvpView().showActivity(activityModels);
                    }

                    @Override
                    public void onError(Throwable t) {

                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }
    @Override
    public List<Integer> findTotalDays(String start_date, String end_date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        long totalDay = 0;

        try {
            Date d1 = format.parse(start_date);
            Date d2 = format.parse(end_date);
            //totalDay = d1.compareTo(d2);
            long diff = Math.abs(d2.getTime() - d1.getTime());
            totalDay = diff / (24 * 60 * 60 * 1000);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return generateSequentialDays((int) totalDay);
    }


    @Override
    public List<Integer> generateSequentialDays(int days) {

        List<Integer> ret = new ArrayList(days  + 1);

        for(int i = 0; i <= days; i++, ret.add(i));

        return ret;
    }

    @Override
    public void storeAttendance(Attendance attendance) {
        getCompositeDisposable().add(getDataRepository().insertAttendance(attendance)
                .observeOn(getSchedulerProvider().ui())
                .subscribeOn(getSchedulerProvider().io())
                .subscribeWith(new DisposableCompletableObserver(){
                    @Override
                    public void onComplete() {
                        getMvpView().openAttendanceFragment();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().showMessage("error" +e);
                    }
                }));
    }

    @Override
    public void onProceedPressed() {

    }

    @Override
    public void onStartDatePressed(Date startDate) {

    }

    @Override
    public void onEndDatePressed(Date EndDate) {

    }

    @Override
    public void onViewPrepared() {

    }
}
