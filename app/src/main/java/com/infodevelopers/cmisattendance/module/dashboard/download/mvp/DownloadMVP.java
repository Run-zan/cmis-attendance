package com.infodevelopers.cmisattendance.module.dashboard.download.mvp;


import com.infodevelopers.cmisattendance.base.presenter.MvpPresenter;
import com.infodevelopers.cmisattendance.base.view.BaseView;


public interface DownloadMVP {
    interface View extends BaseView {

        void updateMasterSize();

        void updateChildSize();

        void updateAdultSize();

        void showHouseHoldDialog();
    }
    interface DwnPresenter<V extends DownloadMVP.View> extends MvpPresenter<V> {

        void startDownlaod();

        void getToatalRegisteredData();
    }

}
