package com.infodevelopers.cmisattendance.module.dashboard.household.mvp;

import android.content.SharedPreferences;

import com.infodevelopers.cmisattendance.base.UserSessionPrefs;
import com.infodevelopers.cmisattendance.network.ApiInterface;
import com.infodevelopers.cmisattendance.utils.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.Observable;

public class HouseholdInteractor {

    SchedulerProvider schedulerProvider;
    ApiInterface apiInterface;
    UserSessionPrefs prefs;


    @Inject
    public HouseholdInteractor(SchedulerProvider schedulerProvider,
                               ApiInterface apiInterface, UserSessionPrefs userSessionPrefs){
        this.schedulerProvider = schedulerProvider;
        this.apiInterface = apiInterface;
        this.prefs = userSessionPrefs;
    }

    public String getLoginResponse(){
        return prefs.getsaveLogin();

    }

//    Observable<SchoolResponse> getSchoolList(String token){
//        return apiInterface.getSchoolList("http://192.168.40.25:8081/git/opmis_sponsorship/api/download",token,"school");
//    }

}
