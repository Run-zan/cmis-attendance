package com.infodevelopers.cmisattendance.module.attendance.wardselect;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.infodevelopers.cmisattendance.R;
import com.infodevelopers.cmisattendance.base.BaseFragment;
import com.infodevelopers.cmisattendance.data.local.model.ActivityModel;
import com.infodevelopers.cmisattendance.data.local.model.Attendance;
import com.infodevelopers.cmisattendance.data.local.model.Child;
import com.infodevelopers.cmisattendance.data.local.model.District;
import com.infodevelopers.cmisattendance.data.local.model.VDC;
import com.infodevelopers.cmisattendance.data.local.model.Ward;
import com.infodevelopers.cmisattendance.module.attendance.AttendanceActivity;
import com.infodevelopers.cmisattendance.module.attendance.attendance_fragment.TakeAttendanceFragment;
import com.infodevelopers.cmisattendance.module.attendance.di.AttendanceComponent;
import com.infodevelopers.cmisattendance.module.attendance.pojo.ExpectedData;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;


public class WardSelectFragment extends BaseFragment implements WardSelectView {
    public static final String TAG = WardSelectFragment.class.getSimpleName();
    @Inject
    WardSelectPresenter<WardSelectView> mPresenter;
    AttendanceComponent attendanceComponent;

    @BindView(R.id.attendance_district_sp)
    Spinner mDistrictSp;

    @BindView(R.id.attendance_ward_sp)
    Spinner mWardSp;

    @BindView(R.id.attendance_vdc_sp)
    Spinner mVdcSp;

    @BindView(R.id.attendance_start_date_et)
    EditText mStartEt;

    @BindView(R.id.attendance_end_date_et)
    EditText mEndEt;

    @BindView(R.id.attendance_activity_sp)
    Spinner mActivitySp;

    @BindView(R.id.attendance_child_number_tv)
    TextView childTv;

    @BindView(R.id.attendance_proceed_btn)
    Button mProceedBtn;


    Context context;
    String mWardId;
    String mDistrictId;
    String mVdcId;
    String mActivityId;

    public static WardSelectFragment newInstance() {

        Bundle args = new Bundle();

        WardSelectFragment fragment = new WardSelectFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_attendance_ward_select, container, false);
        setUnBinder(ButterKnife.bind(this, view));
        initializeDependencies(view);
        context = getActivity();
        mPresenter.getDistrict();
        mPresenter.getAllActivity();

        DatePickerDialog.OnDateSetListener startDateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                mStartEt.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
            }
        };

        DatePickerDialog.OnDateSetListener endDateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                mEndEt.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
            }
        };
        mDistrictSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                District district = (District) adapterView.getItemAtPosition(i);
                mDistrictId = district.getDISTRICTID();
                mPresenter.getVdc(Integer.parseInt(district.getDISTRICTID()));

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mVdcSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                VDC vdc = (VDC) adapterView.getItemAtPosition(i);
                mVdcId = vdc.getmVDCID();
                mPresenter.getWard(Integer.parseInt(vdc.getmVDCID()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        mWardSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Ward ward = (Ward) adapterView.getItemAtPosition(i);
                mWardId = ward.getWARDID();
                mPresenter.getChildData(mDistrictId, mVdcId, mWardId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mStartEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SpinnerDatePickerDialogBuilder()
                        .context(getActivity())
                        .callback(startDateListener)
                        .showTitle(true)
                        .showDaySpinner(true)
                        .defaultDate(2019, 0, 1)
                        .maxDate(2020, 0, 1)
                        .minDate(2000, 0, 1)
                        .build()
                        .show();
            }
        });

        mEndEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SpinnerDatePickerDialogBuilder()
                        .context(getActivity())
                        .callback(endDateListener)
                        .showTitle(true)
                        .showDaySpinner(true)
                        .defaultDate(2019, 0, 1)
                        .maxDate(2020, 0, 1)
                        .minDate(2000, 0, 1)
                        .build()
                        .show();
            }
        });

//        mEndEt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new SpinnerDatePickerDialogBuilder()
//                        .context(getActivity())
//                        .callback(WardSelectFragment.this)
//                        .showTitle(true)
//                        .showDaySpinner(true)
//                        .defaultDate(2019, 0, 1)
//                        .maxDate(2020, 0, 1)
//                        .minDate(2000, 0, 1)
//                        .build()
//                        .show();
//            }
//        });
        mActivitySp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ActivityModel activity = (ActivityModel) adapterView.getItemAtPosition(i);
                mActivityId = activity.getActivity_id();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mProceedBtn.setOnClickListener(view1 -> {

            String startDate = mStartEt.getText().toString();
            String endDate = mEndEt.getText().toString();
            int totaldays = mPresenter.findTotalDays(startDate, endDate).size();

            Attendance attendance = new Attendance(startDate, endDate, totaldays, mActivityId,mDistrictId,mVdcId,mWardId );
            mPresenter.storeAttendance(attendance);
            //  ExpectedData mExpectedData=new ExpectedData(mWardId,mVdcId,mDistrictId,startDate,endDate,mActivityId);
            //  openAttendanceFragment();
        });

        return view;
    }

    private void initializeDependencies(View v) {
        AttendanceActivity attendanceActivity = (AttendanceActivity) getActivity();
        attendanceComponent = attendanceActivity.getAttendanceComponent();
        if (attendanceComponent != null) {
            attendanceComponent.inject(this);
            mPresenter.onAttach(this);
            setUnBinder(ButterKnife.bind(this, v));

        }
    }

    @Override
    public void showDistrict(List<District> districtList) {
        ArrayAdapter<District> adapter = new ArrayAdapter<District>(context,
                android.R.layout.simple_spinner_dropdown_item, districtList);
        mDistrictSp.setAdapter(adapter);
    }

    @Override
    public void showVdc(List<VDC> vdcList) {
        ArrayAdapter<VDC> adapter = new ArrayAdapter<VDC>(context,
                android.R.layout.simple_spinner_dropdown_item, vdcList);
        mVdcSp.setAdapter(adapter);
    }

    @Override
    public void showWard(List<Ward> wardList) {
        ArrayAdapter<Ward> adapter = new ArrayAdapter<Ward>(context,
                android.R.layout.simple_spinner_dropdown_item, wardList);
        mWardSp.setAdapter(adapter);
    }

    @Override
    public void showActivity(List<ActivityModel> activityList) {
        ArrayAdapter<ActivityModel> adapter = new ArrayAdapter<ActivityModel>(context,
                android.R.layout.simple_spinner_dropdown_item, activityList);
        mActivitySp.setAdapter(adapter);
    }

    @Override
    public void showChildData(List<Child> childList) {
        String text = (childList.size() != 0) ? (childList.size() + " Child record") : "No Child for the selected ward.";
        childTv.setText(text);
    }

    @Override
    public void openAttendanceFragment() {
        String startDate = mStartEt.getText().toString();
        String endDate = mEndEt.getText().toString();

        ExpectedData mExpectedData = new ExpectedData(mWardId, mVdcId, mDistrictId, startDate, endDate, mActivityId);
        TakeAttendanceFragment takeAttendanceFragment = TakeAttendanceFragment.newInstance(mExpectedData);


        if (getActivity() instanceof AttendanceActivity) {
            AttendanceActivity attendanceActivity = (AttendanceActivity) getActivity();
            attendanceActivity.openTakeAttendanceFragment(takeAttendanceFragment);
            //Open new fragment
        }
    }

    @Override
    protected void setUp(View view) {


    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void initialize() {

    }


}
