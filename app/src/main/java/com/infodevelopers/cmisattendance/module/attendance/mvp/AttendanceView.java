package com.infodevelopers.cmisattendance.module.attendance.mvp;

import com.infodevelopers.cmisattendance.base.view.BaseView;
import com.infodevelopers.cmisattendance.module.attendance.attendance_fragment.TakeAttendanceFragment;
import com.infodevelopers.cmisattendance.module.attendance.di.AttendanceComponent;


public interface AttendanceView extends BaseView {

    AttendanceComponent getAttendanceComponent();
    void openTakeAttendanceFragment(TakeAttendanceFragment takeAttendanceFragment);

}
