package com.infodevelopers.cmisattendance.module.attendance.wardselect;

import com.infodevelopers.cmisattendance.base.view.BaseView;
import com.infodevelopers.cmisattendance.data.local.model.ActivityModel;
import com.infodevelopers.cmisattendance.data.local.model.Child;
import com.infodevelopers.cmisattendance.data.local.model.District;
import com.infodevelopers.cmisattendance.data.local.model.VDC;
import com.infodevelopers.cmisattendance.data.local.model.Ward;

import java.util.List;

public interface WardSelectView extends BaseView {

    void showDistrict(List<District> districtList);
    void showVdc(List<VDC> vdcList);
    void showWard(List<Ward> wardList);
    void showActivity(List<ActivityModel> activityList);
    void showChildData(List<Child> childList);
    void openAttendanceFragment();


}
