package com.infodevelopers.cmisattendance.module.dashboard.download.di;

import com.infodevelopers.cmisattendance.di.component.ApplicationComponent;
import com.infodevelopers.cmisattendance.di.scope.PerActivity;
import com.infodevelopers.cmisattendance.module.dashboard.MainActivity;
import com.infodevelopers.cmisattendance.module.dashboard.household.HouseholdDialogFragment;

import dagger.Component;

@PerActivity
@Component(modules = DownloadModule.class,dependencies = ApplicationComponent.class)
public interface DownloadComponent {
    void inject(MainActivity mainActivity);
    void inject(HouseholdDialogFragment householdDialogFragment);
}
