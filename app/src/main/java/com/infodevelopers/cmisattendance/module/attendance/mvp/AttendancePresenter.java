package com.infodevelopers.cmisattendance.module.attendance.mvp;

import com.infodevelopers.cmisattendance.base.presenter.MvpPresenter;

public interface AttendancePresenter<V extends AttendanceView> extends MvpPresenter<V> {


}
