package com.infodevelopers.cmisattendance.module.login.di;



import com.infodevelopers.cmisattendance.di.scope.PerActivity;
import com.infodevelopers.cmisattendance.module.login.di.interactor.LoginInteractor;
import com.infodevelopers.cmisattendance.module.login.mvp.LoginPresenter;
import com.infodevelopers.cmisattendance.module.login.mvp.LoginPresenterImpl;
import com.infodevelopers.cmisattendance.module.login.mvp.LoginView;

import dagger.Module;
import dagger.Provides;


@Module

public class LoginModule {

    @Provides
    @PerActivity
    LoginPresenter<LoginView> provideLoginPresenter(LoginPresenterImpl<LoginView> presenter){
        return presenter;
    }





}

