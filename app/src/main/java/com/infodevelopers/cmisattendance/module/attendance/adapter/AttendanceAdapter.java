package com.infodevelopers.cmisattendance.module.attendance.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import com.google.gson.Gson;
import com.infodevelopers.cmisattendance.R;
import com.infodevelopers.cmisattendance.data.local.model.ChildAttendance;
import java.util.ArrayList;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;


public class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.ViewHolder>{

    Context mContext;
    List<ChildAttendance> data ;
    LayoutInflater mInflater;
    String day;

    @NonNull
    private OnItemCheckListener onItemCheckListener;

    public AttendanceAdapter(Context context, List<ChildAttendance> childList, @NonNull OnItemCheckListener onItemCheckListener) {
        this.data = childList;
        this.mContext = context;
        this.onItemCheckListener = onItemCheckListener;
        this.mInflater = LayoutInflater.from(mContext);
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public interface OnItemCheckListener {
        void onItemCheck(ChildAttendance child, CheckBox view, String day);
        void onItemUncheck(ChildAttendance child, CheckBox view, String day);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.attendance_single_row, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String name = data.get(position).getChildName();
        holder.mName.setText(name);

        if(holder instanceof ViewHolder){
            final ChildAttendance child = data.get(position);

            ((ViewHolder) holder).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ArrayList<String> presentList= child.getPresent_list();
                    ((ViewHolder) holder).mCheckBox.setChecked(
                            !((ViewHolder) holder).mCheckBox.isChecked());
                    if (((ViewHolder) holder).mCheckBox.isChecked()) {

                        //day is provided by spinner through getter method
                        if(!presentList.contains(day)){
                        presentList.add(day);
                          data.get(position).setPresent_list(presentList);
                        }
                        onItemCheckListener.onItemCheck(child, holder.mCheckBox, day);

                    } else {
                        onItemCheckListener.onItemUncheck(child, holder.mCheckBox, day);
                        if(presentList.contains(day))
                        {
                            presentList.remove(day);
                            data.get(position).setPresent_list(presentList);
                        }
                    }

                    Log.d("test",new Gson().toJson(data));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.attendance_single_linearlayout)
        CardView mLayout;

        @BindView(R.id.attendance_single_name)
        TextView mName;

        @BindView(R.id.attendance_single_cb)
        CheckBox mCheckBox;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            mCheckBox.setClickable(false);
        }

        public void setOnClickListener(View.OnClickListener onClickListener) {
            itemView.setOnClickListener(onClickListener);
        }
    }
}
