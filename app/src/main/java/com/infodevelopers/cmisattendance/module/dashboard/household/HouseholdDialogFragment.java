package com.infodevelopers.cmisattendance.module.dashboard.household;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.infodevelopers.cmisattendance.R;
import com.infodevelopers.cmisattendance.app.MainApp;
import com.infodevelopers.cmisattendance.data.local.model.District;
import com.infodevelopers.cmisattendance.data.local.model.VDC;
import com.infodevelopers.cmisattendance.data.local.model.Ward;
import com.infodevelopers.cmisattendance.module.dashboard.download.di.DaggerDownloadComponent;
import com.infodevelopers.cmisattendance.module.dashboard.download.di.DownloadComponent;
import com.infodevelopers.cmisattendance.module.dashboard.download.di.DownloadModule;
import com.infodevelopers.cmisattendance.module.dashboard.household.mvp.HouseholdPresenter;
import com.infodevelopers.cmisattendance.module.dashboard.household.mvp.HouseholdView;

import java.util.List;
import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HouseholdDialogFragment extends AppCompatDialogFragment implements View.OnClickListener,HouseholdView {
Context context;
    @Inject
    HouseholdPresenter<HouseholdView> mPresenter;

    DownloadComponent downloadComponent;
    MaterialDialog mDialog;

    @BindView(R.id.householdmvp_spinner_district)
     Spinner mSpinnerDis;

    @BindView(R.id.householdmvp_spinner_vdc)
     Spinner mSpinnerVdc;



    @BindView(R.id.householdmvp_spinner_ward)
      Spinner mSpinnerWard;

    int wardId;
    int districtId;
    int vdcId;
    int wardUniqueId;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.household_download_dialog,container,false);
        ButterKnife.bind(this,view);
        mPresenter.onAttach(this);
        context=getActivity();



        mPresenter.getDistrict();
                mSpinnerDis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                District district=(District) adapterView.getItemAtPosition(i);
                districtId=Integer.parseInt(district.getDISTRICTID());
                mPresenter.getVdcForDistrict(Integer.parseInt(district.getDISTRICTID()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
                mSpinnerVdc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        VDC vdc=(VDC) adapterView.getItemAtPosition(i);
                        vdcId=Integer.parseInt(vdc.getVDCID());
                        mPresenter.getWardForVdc(Integer.parseInt(vdc.getVDCID()));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                mSpinnerWard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        Ward ward=(Ward) adapterView.getItemAtPosition(i);
                        wardUniqueId=ward.getId();
                        wardId=Integer.parseInt(ward.getWARDID());

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
        return view;
    }
@OnClick(R.id.householdmvp_btn_fetch)
void downloadChildData(){
    Log.d("download","ward_id: "+wardId+" "+"vdc_id: "+vdcId+" districtId: "+districtId);
     mPresenter.fetchChildData(districtId,vdcId,wardId);

}
    @Override
    public void onAttach(@NonNull Context context) {

        super.onAttach(context);
        this.context=context;
        MainApp mainApp=(MainApp) getActivity().getApplicationContext();
        downloadComponent=DaggerDownloadComponent.builder()
                .applicationComponent(mainApp.getApplicationComponent())
                .downloadModule(new DownloadModule())
                .build();
        downloadComponent.inject(this);

    }
    //    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//
//    }

    //   @Override
//    public void onViewReady(Bundle savedInstances, Intent intent) {
//        super.onViewReady(savedInstances, intent);
//        initilizeDependency();
//        setUnBinder(ButterKnife.bind(this));
//        mPresenter.onAttach(HouseholdDialogFragment.this);
//        mPresenter.getDistrict();
//
//    }

//    private void initilizeDependency() {
//        MainApp mainApp=(MainApp) getApplicationContext();
//
//            downloadComponent=DaggerDownloadComponent.builder()
//                    .applicationComponent(mainApp.getApplicationComponent())
//                    .downloadModule(new DownloadModule())
//                    .build();
//            downloadComponent.inject(this);
//
//
//    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.householdmvp_btn_fetch:
              //  mHouseholdPresenter.fetchData();
        }

    }


    @Override
    public void showDistrict(List<District> districtList) {
        ArrayAdapter<District> adapter = new ArrayAdapter<District>(context,
                android.R.layout.simple_spinner_dropdown_item,districtList);
        mSpinnerDis.setAdapter(adapter);
    }

    @Override
    public void showWard(List<Ward> wardList) {
        ArrayAdapter<Ward> adapter = new ArrayAdapter<Ward>(context,
                android.R.layout.simple_spinner_dropdown_item,wardList);
        mSpinnerWard.setAdapter(adapter);
    }

    @Override
    public void showVdc(List<VDC> vdcList) {
        ArrayAdapter<VDC> adapter = new ArrayAdapter<VDC>(context,
                android.R.layout.simple_spinner_dropdown_item,vdcList);
        mSpinnerVdc.setAdapter(adapter);
    }




//    @Override
//    protected int getContentView() {
//        return R.layout.household_download_dialog;
//    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mSpinnerWard.setAdapter(null);
        mSpinnerVdc.setAdapter(null);
        mSpinnerWard.setAdapter(null);
    }

    @Override
    public void initialize() {

    }

    @Override
    public void showLoading(String message) {
        if(mDialog==null) {
            mDialog = new MaterialDialog.Builder(getActivity())
                    .cancelable(true)
                    .autoDismiss(false)
                    .content(message)
                    .progress(true, 0).build();

        }
        mDialog.show();

    }

    @Override
    public void hideLoading() {
        if(mDialog!=null && mDialog.isShowing()){
            mDialog.dismiss();
        }
    }

    @Override
    public void openActivityOnTokenExpire() {

    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(context,message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showMessage(int resId) {

    }

    @Override
    public boolean isNetworkConnected() {
        return false;
    }

    @Override
    public void hideKeyboard() {

    }


}
