package com.infodevelopers.cmisattendance.module.attendance.wardselect;

import com.infodevelopers.cmisattendance.base.presenter.MvpPresenter;
import com.infodevelopers.cmisattendance.base.view.BaseView;
import com.infodevelopers.cmisattendance.data.local.model.Attendance;

import java.util.Date;
import java.util.List;

public interface WardSelectPresenter<V extends BaseView> extends MvpPresenter<V> {
    void getDistrict();
    void getVdc(int district_id);
    void getWard(int vdc_id);
    void getChildData(String district_id,String vdc_id,String ward_id);
    void getAllActivity();
    List<Integer> findTotalDays(String start_date, String end_date);
    List<Integer> generateSequentialDays(int days);
    void storeAttendance(Attendance attendance);
    void onProceedPressed();
    void onStartDatePressed(Date startDate);
    void onEndDatePressed(Date EndDate);
    void onViewPrepared();
}
