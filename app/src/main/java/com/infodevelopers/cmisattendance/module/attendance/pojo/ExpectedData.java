package com.infodevelopers.cmisattendance.module.attendance.pojo;


import android.os.Parcel;
import android.os.Parcelable;

public class ExpectedData implements Parcelable {

    String  ward_name;
    String vdc_id;
    String district_id;
    String start_date;
    String end_date;
    String activity_id;

    protected ExpectedData(Parcel in) {
        ward_name = in.readString();
        vdc_id = in.readString();
        district_id = in.readString();
        start_date = in.readString();
        end_date = in.readString();
        activity_id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ward_name);
        dest.writeString(vdc_id);
        dest.writeString(district_id);
        dest.writeString(start_date);
        dest.writeString(end_date);
        dest.writeString(activity_id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ExpectedData> CREATOR = new Creator<ExpectedData>() {
        @Override
        public ExpectedData createFromParcel(Parcel in) {
            return new ExpectedData(in);
        }

        @Override
        public ExpectedData[] newArray(int size) {
            return new ExpectedData[size];
        }
    };

    public ExpectedData(String ward_name, String vdc_id, String district_id, String start_date, String end_date, String activity_id) {
        this.ward_name = ward_name;
        this.vdc_id = vdc_id;
        this.district_id = district_id;
        this.start_date = start_date;
        this.end_date = end_date;
        this.activity_id = activity_id;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(String activity_id) {
        this.activity_id = activity_id;
    }


    public String getWard_name() {
        return ward_name;
    }

    public void setWard_name(String ward_name) {
        this.ward_name = ward_name;
    }



    public String getVdc_id() {
        return vdc_id;
    }

    public void setVdc_id(String vdc_id) {
        this.vdc_id = vdc_id;
    }

    public String getDistrict_id() {
        return district_id;
    }

    public void setDistrict_id(String district_id) {
        this.district_id = district_id;
    }

}
