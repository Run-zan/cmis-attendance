package com.infodevelopers.cmisattendance.module.attendance.wardselect;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.infodevelopers.cmisattendance.data.local.model.Ward;

import java.util.List;

import androidx.annotation.NonNull;

public class WardSpinnerAdapter extends ArrayAdapter<Ward> {

    public WardSpinnerAdapter(@NonNull Context context, int resource, @NonNull List<Ward> objects) {
        super(context, resource, objects);
    }

}
