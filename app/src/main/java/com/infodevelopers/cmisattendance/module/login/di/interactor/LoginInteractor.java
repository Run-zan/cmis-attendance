package com.infodevelopers.cmisattendance.module.login.di.interactor;


import android.content.Context;
import android.util.Log;
import com.infodevelopers.cmisattendance.base.UserSessionPrefs;
import com.infodevelopers.cmisattendance.di.scope.PerActivity;
import com.infodevelopers.cmisattendance.module.login.extras.SHA256;
import com.infodevelopers.cmisattendance.module.login.pojo.LoginResponse;
import com.infodevelopers.cmisattendance.network.ApiInterface;
import com.infodevelopers.cmisattendance.utils.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.Observable;
@PerActivity
public class LoginInteractor {

    private SchedulerProvider schedulerProvider;
    private ApiInterface apiService;
    private UserSessionPrefs userSessionPrefs;

    @Inject
    public LoginInteractor(SchedulerProvider schedulerProvider,
                           ApiInterface  apiService,
                           UserSessionPrefs userSessionPrefs) {
        this.schedulerProvider = schedulerProvider;
        this.apiService = apiService;
        this.userSessionPrefs = userSessionPrefs;
    }

    public Observable<LoginResponse> loginOnline(Context c, String username, String password) {

        return apiService.login(
                username,
                getUserType("Staff"),
                getEncrypted(getUserType("Staff") + username + password))
                .concatMap(loginResponse -> {
                    if (loginResponse.getStatus().equals("success")) {

                        userSessionPrefs.saveLogin(loginResponse);
                    }
                    return Observable.just(loginResponse);
                });

    }
    private String getEncrypted(String s) {
        String hash=SHA256.getEncryptedValue(s);
        Log.d("hash",hash);
        return hash;
    }


    private String getUserType(String s) {
        if (s.equalsIgnoreCase("Staff")) {
            return "1";
        } else {
            return "2";
        }
    }

}
