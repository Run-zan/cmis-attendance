package com.infodevelopers.cmisattendance.module.attendance.di;


import com.infodevelopers.cmisattendance.di.component.ApplicationComponent;
import com.infodevelopers.cmisattendance.di.scope.PerActivity;
import com.infodevelopers.cmisattendance.module.attendance.AttendanceActivity;
import com.infodevelopers.cmisattendance.module.attendance.attendance_fragment.TakeAttendanceFragment;
import com.infodevelopers.cmisattendance.module.attendance.wardselect.WardSelectFragment;
import dagger.Component;

@PerActivity
@Component(
        modules = AttendanceModule.class, dependencies = ApplicationComponent.class
)
public interface AttendanceComponent {
    void inject(AttendanceActivity attendanceActivity);
    void inject(WardSelectFragment wardSelectFragment);
    void inject(TakeAttendanceFragment attendanceFragment);
}
