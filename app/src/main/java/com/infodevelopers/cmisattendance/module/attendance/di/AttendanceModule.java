package com.infodevelopers.cmisattendance.module.attendance.di;


import com.infodevelopers.cmisattendance.di.scope.PerActivity;
import com.infodevelopers.cmisattendance.module.attendance.attendance_fragment.TakeAttendanceImpl;
import com.infodevelopers.cmisattendance.module.attendance.attendance_fragment.TakeAttendancePresenter;
import com.infodevelopers.cmisattendance.module.attendance.attendance_fragment.TakeAttendanceView;
import com.infodevelopers.cmisattendance.module.attendance.mvp.AttendancePresenter;
import com.infodevelopers.cmisattendance.module.attendance.mvp.AttendancePresenterImpl;
import com.infodevelopers.cmisattendance.module.attendance.mvp.AttendanceView;
import com.infodevelopers.cmisattendance.module.attendance.wardselect.WardSelectPresenter;
import com.infodevelopers.cmisattendance.module.attendance.wardselect.WardSelectPresenterImpl;
import com.infodevelopers.cmisattendance.module.attendance.wardselect.WardSelectView;

import dagger.Module;
import dagger.Provides;

@Module
public class AttendanceModule {

    @PerActivity
    @Provides
    AttendancePresenter<AttendanceView> provideAttendancePresenter(AttendancePresenterImpl<AttendanceView> attendancePresenter){
        return attendancePresenter;
    }

    @PerActivity
    @Provides
    WardSelectPresenter<WardSelectView> provideWardPresnter(WardSelectPresenterImpl<WardSelectView> wardSelectPresenter){
        return wardSelectPresenter;
    }

    @PerActivity
    @Provides
    TakeAttendancePresenter<TakeAttendanceView> providesAttendanceFragView(TakeAttendanceImpl<TakeAttendanceView> attendanceFragPresenter){
        return attendanceFragPresenter;
    }

}
