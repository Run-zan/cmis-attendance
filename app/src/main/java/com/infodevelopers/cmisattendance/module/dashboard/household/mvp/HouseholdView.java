package com.infodevelopers.cmisattendance.module.dashboard.household.mvp;

import com.infodevelopers.cmisattendance.base.view.BaseView;
import com.infodevelopers.cmisattendance.data.local.model.District;
import com.infodevelopers.cmisattendance.data.local.model.VDC;
import com.infodevelopers.cmisattendance.data.local.model.Ward;

import java.util.List;

import io.reactivex.Flowable;

public interface HouseholdView extends BaseView {
    void showDistrict(List<District> districtList);
    void showWard(List<Ward>wardList);
    void showVdc(List<VDC> vdcList);

}
