package com.infodevelopers.cmisattendance.module.login.mvp;

import com.infodevelopers.cmisattendance.base.view.BaseView;

public interface LoginView extends BaseView {
    void openMainActvity();
}
