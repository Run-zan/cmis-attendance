package com.infodevelopers.cmisattendance.module.attendance.attendance_fragment;


import com.infodevelopers.cmisattendance.base.presenter.MvpPresenter;

import java.util.List;

public interface TakeAttendancePresenter<V extends TakeAttendanceView> extends MvpPresenter<V> {

    void getChildData(String district_id, String vdc_id, String ward_name);
    List<Integer> findTotalDays(String start_date, String end_date);
    List<Integer> generateSequentialDays(int days);

}
