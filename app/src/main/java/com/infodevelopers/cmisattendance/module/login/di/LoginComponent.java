package com.infodevelopers.cmisattendance.module.login.di;

import com.infodevelopers.cmisattendance.di.component.ApplicationComponent;
import com.infodevelopers.cmisattendance.di.scope.PerActivity;
import com.infodevelopers.cmisattendance.di.scope.PerApp;
import com.infodevelopers.cmisattendance.module.login.mvp.LoginActivity;

import javax.inject.Singleton;

import dagger.Component;
import dagger.Subcomponent;

@PerActivity
@Component(modules = LoginModule.class,dependencies = ApplicationComponent.class)
public interface LoginComponent {
    void inject(LoginActivity activity);
}

