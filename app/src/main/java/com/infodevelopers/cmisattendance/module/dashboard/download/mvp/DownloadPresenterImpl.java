package com.infodevelopers.cmisattendance.module.dashboard.download.mvp;

import android.util.Log;
import com.infodevelopers.cmisattendance.base.presenter.BasePresenter;
import com.infodevelopers.cmisattendance.data.repository.DataRepository;
import com.infodevelopers.cmisattendance.utils.SchedulerProvider;
import javax.inject.Inject;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;


public class DownloadPresenterImpl<V extends DownloadMVP.View> extends BasePresenter<V> implements DownloadMVP.DwnPresenter<V> {

    @Inject
    public DownloadPresenterImpl(
            SchedulerProvider schedulerProvider, DataRepository dataRepository, CompositeDisposable compositeDisposable) {
        super(schedulerProvider, compositeDisposable, dataRepository);

        initialize();
    }

    private void initialize() {
        // view.setPresenter(this);
        getLoginResponse();
    }

    private void getLoginResponse() {
//        loginResponse =  new Gson().fromJson(interactor.getLoginResponse(), new TypeToken<LoginResponse>(){
//
//
//        }.getType());
    }

    @Override
    public void startDownlaod() {


        getMvpView().showLoading("downloading...");
        getCompositeDisposable().add(getDataRepository().getDownloadData()
                .subscribeOn(getSchedulerProvider().io())
                .concatMapCompletable(downloadWrapper -> getDataRepository().storeDownloadData(downloadWrapper))
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        getMvpView().showMessage("SuccessFully Downloaded Data");
                        getMvpView().hideLoading();
                        getMvpView().showHouseHoldDialog();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("error",e.toString());
                    }
                }));
    }


    @Override
    public void getToatalRegisteredData() {

    }

    @Override
    public void subscribe() {
//        if(disposable==null){
//           disposable = getCompositeDisposable();
//        }

    }

    @Override
    public void unSubscribe() {
        //  disposable.clear();
    }
}
