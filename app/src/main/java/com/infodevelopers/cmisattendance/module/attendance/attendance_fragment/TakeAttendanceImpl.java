package com.infodevelopers.cmisattendance.module.attendance.attendance_fragment;

import android.util.Log;

import com.infodevelopers.cmisattendance.base.presenter.BasePresenter;
import com.infodevelopers.cmisattendance.data.local.model.Child;
import com.infodevelopers.cmisattendance.data.repository.DataRepository;
import com.infodevelopers.cmisattendance.utils.SchedulerProvider;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subscribers.DisposableSubscriber;


public class TakeAttendanceImpl<V extends TakeAttendanceView> extends BasePresenter<V> implements TakeAttendancePresenter<V> {

    @Inject
    public TakeAttendanceImpl(SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable, DataRepository dataRepository) {
        super(schedulerProvider, compositeDisposable, dataRepository);
    }

    @Override
    public void getChildData(String district_id, String vdc_id, String ward_name) {

        getCompositeDisposable().add(getDataRepository().getChildData(district_id, vdc_id, ward_name)
                .observeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSubscriber<List<Child>>() {
                @Override
                public void onNext(List<Child> childList) {
                    getMvpView().setAdapter(null);

                }

                @Override
                public void onError(Throwable t) {
                    getMvpView().showMessage("Error loadind data");

                }

                @Override
                public void onComplete() {

                }
            }));
    }


    @Override
    public List<Integer> findTotalDays(String start_date, String end_date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        long totalDay = 0;

        try {
            Date d1 = format.parse(start_date);
            Date d2 = format.parse(end_date);
            //totalDay = d1.compareTo(d2);
            long diff = Math.abs(d2.getTime() - d1.getTime());
             totalDay = diff / (24 * 60 * 60 * 1000);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return generateSequentialDays((int) totalDay);
    }


    @Override
    public List<Integer> generateSequentialDays(int days) {

        List<Integer> ret = new ArrayList(days  + 1);

        for(int i = 0; i <= days; i++, ret.add(i));

        return ret;
    }
}
