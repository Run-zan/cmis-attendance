package com.infodevelopers.cmisattendance.module.attendance.pojo;

import com.infodevelopers.cmisattendance.data.local.model.Ward;

public  class  DownloadedWard extends Ward  {
    public String mName;

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public Boolean downloaded;

    public Boolean getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(Boolean downloaded) {
        this.downloaded = downloaded;
    }
}
