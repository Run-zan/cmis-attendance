package com.infodevelopers.cmisattendance.module.dashboard.download.di;

import com.infodevelopers.cmisattendance.di.scope.PerActivity;
import com.infodevelopers.cmisattendance.module.dashboard.download.mvp.DownloadMVP;
import com.infodevelopers.cmisattendance.module.dashboard.download.mvp.DownloadPresenterImpl;
import com.infodevelopers.cmisattendance.module.dashboard.household.mvp.HouseholdPresenter;
import com.infodevelopers.cmisattendance.module.dashboard.household.mvp.HouseholdPresenterImpl;
import com.infodevelopers.cmisattendance.module.dashboard.household.mvp.HouseholdView;

import dagger.Module;
import dagger.Provides;

@Module
public class DownloadModule  {


    @Provides
    @PerActivity
    DownloadMVP.DwnPresenter<DownloadMVP.View> provideDownloadPresenter(DownloadPresenterImpl<DownloadMVP.View> downloadPresenter){
        return downloadPresenter;
    }


    @PerActivity
    @Provides
    HouseholdPresenter<HouseholdView> provideHouseHoldPresenter(HouseholdPresenterImpl<HouseholdView> presenter){
        return presenter;
    }



}
