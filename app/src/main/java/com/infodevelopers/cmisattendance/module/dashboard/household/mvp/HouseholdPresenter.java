package com.infodevelopers.cmisattendance.module.dashboard.household.mvp;

import com.infodevelopers.cmisattendance.base.presenter.MvpPresenter;
import com.infodevelopers.cmisattendance.data.local.model.District;
import com.infodevelopers.cmisattendance.data.local.model.VDC;
import com.infodevelopers.cmisattendance.data.local.model.Ward;
import com.infodevelopers.cmisattendance.di.scope.PerActivity;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;

@PerActivity
public interface HouseholdPresenter<V extends HouseholdView> extends MvpPresenter<V>  {
    List<District> getDistrict();
    List<VDC> getVdcForDistrict(int district_id);
    List<Ward> getWardForVdc(int vdc_id);
    void fetchChildData(int districtId, int vdcId, int wardId);

}
