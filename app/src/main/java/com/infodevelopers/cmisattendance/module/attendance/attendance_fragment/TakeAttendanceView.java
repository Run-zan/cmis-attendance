package com.infodevelopers.cmisattendance.module.attendance.attendance_fragment;

import com.infodevelopers.cmisattendance.base.view.BaseView;
import com.infodevelopers.cmisattendance.data.local.model.ChildAttendance;

import java.util.List;

public interface TakeAttendanceView extends BaseView {

    void setAdapter(List<ChildAttendance> childList);
    void openMainActivity();
}
