package com.infodevelopers.cmisattendance.module.attendance;

import android.content.Intent;
import android.os.Bundle;
import com.infodevelopers.cmisattendance.R;
import com.infodevelopers.cmisattendance.app.MainApp;
import com.infodevelopers.cmisattendance.base.BaseActivity;
import com.infodevelopers.cmisattendance.module.attendance.adapter.AttendanceAdapter;
import com.infodevelopers.cmisattendance.module.attendance.attendance_fragment.TakeAttendanceFragment;
import com.infodevelopers.cmisattendance.module.attendance.di.AttendanceComponent;
import com.infodevelopers.cmisattendance.module.attendance.di.AttendanceModule;
import com.infodevelopers.cmisattendance.module.attendance.di.DaggerAttendanceComponent;
import com.infodevelopers.cmisattendance.module.attendance.mvp.AttendancePresenter;
import com.infodevelopers.cmisattendance.module.attendance.mvp.AttendanceView;
import com.infodevelopers.cmisattendance.module.attendance.wardselect.WardSelectFragment;
import javax.inject.Inject;


public class AttendanceActivity extends BaseActivity implements AttendanceView {

    AttendanceAdapter attendanceAdapter;

    @Inject
    AttendancePresenter<AttendanceView> attendancePresenter;
    AttendanceComponent attendanceComponent;

//    @BindView(R.id.attendance_container)
//    FrameLayout mContainer;

    @Override
    protected int getContentView() {
        return R.layout.activity_attendance;
    }

    @Override
    public void onViewReady(Bundle savedInstances, Intent intent) {
        super.onViewReady(savedInstances, intent);

        initializeDependency();
        attendancePresenter.onAttach(this);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.attendance_container, WardSelectFragment.newInstance(), WardSelectFragment.TAG)
                .commit();

    }

    private void initializeDependency() {
        MainApp mainApp = (MainApp) getApplicationContext();
        attendanceComponent = DaggerAttendanceComponent.builder()
                .applicationComponent(mainApp.getApplicationComponent())
                .attendanceModule(new AttendanceModule())
                .build();

        attendanceComponent.inject(this);

    }


    @Override
    public void initialize() {

    }

    @Override
    public void showMessage(int resId) {

    }


    @Override
    public AttendanceComponent getAttendanceComponent() {
        return attendanceComponent;
    }

    @Override
    public void openTakeAttendanceFragment(TakeAttendanceFragment takeAttendanceFragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.attendance_container, takeAttendanceFragment, TakeAttendanceFragment.TAG)
                .commit();

    }

    @Override
    protected void onDestroy() {
        attendancePresenter.onDetach();
        super.onDestroy();

    }
}



