package com.infodevelopers.cmisattendance.module.attendance.mvp;

import com.infodevelopers.cmisattendance.base.presenter.BasePresenter;
import com.infodevelopers.cmisattendance.data.local.model.Child;
import com.infodevelopers.cmisattendance.data.repository.DataRepository;
import com.infodevelopers.cmisattendance.utils.SchedulerProvider;
import java.util.List;
import javax.inject.Inject;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subscribers.DisposableSubscriber;

public class AttendancePresenterImpl<V extends AttendanceView> extends BasePresenter<V> implements AttendancePresenter<V> {


    @Inject
    public AttendancePresenterImpl(SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable, DataRepository dataRepository) {
        super(schedulerProvider, compositeDisposable, dataRepository);
    }

}
