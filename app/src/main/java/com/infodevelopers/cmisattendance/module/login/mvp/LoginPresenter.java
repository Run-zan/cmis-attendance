package com.infodevelopers.cmisattendance.module.login.mvp;

import android.content.Context;

import com.infodevelopers.cmisattendance.base.presenter.MvpPresenter;
import com.infodevelopers.cmisattendance.di.scope.PerActivity;

@PerActivity
public interface LoginPresenter <V extends LoginView> extends MvpPresenter<V> {
    void loginUser(Context c, String username, String password);
}
