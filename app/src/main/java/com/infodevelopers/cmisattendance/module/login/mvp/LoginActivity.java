package com.infodevelopers.cmisattendance.module.login.mvp;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import com.afollestad.materialdialogs.MaterialDialog;
import com.infodevelopers.cmisattendance.R;
import com.infodevelopers.cmisattendance.app.MainApp;
import com.infodevelopers.cmisattendance.base.BaseActivity;
import com.infodevelopers.cmisattendance.base.UserSessionPrefs;
import com.infodevelopers.cmisattendance.module.attendance.mvp.AttendancePresenterImpl;
import com.infodevelopers.cmisattendance.module.dashboard.MainActivity;
import com.infodevelopers.cmisattendance.module.login.di.DaggerLoginComponent;
import com.infodevelopers.cmisattendance.module.login.di.LoginComponent;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class LoginActivity extends BaseActivity implements LoginView {
    MaterialDialog mDialog;

    @Inject
    LoginPresenter<LoginView> mPresenter;

  //  Login.Loginpresenter presenter;

    LoginComponent loginComponent;

    @BindView(R.id.activity_login_et_username)
    EditText mUserName;

    @BindView(R.id.activity_login_et_password)
    EditText mPassword;

    @BindView(R.id.activity_login_btn_login)
    Button mSubmitButton;

    UserSessionPrefs userSessionPrefs;


    @Override
    protected int getContentView() {
        return R.layout.activity_login;
    }

    @Override
    public void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);
        userSessionPrefs = new UserSessionPrefs(this);
        setUnBinder(ButterKnife.bind(this));
         /*initialize dependencies*/
        initializeDependencies();
        mPresenter.onAttach(LoginActivity.this);
     //   final WebSettings webSettings = tvDatas.getSettings();
     //   Resources res = getResources();
     //   fontSize = res.getDimension(R.dimen.txtSize);
     //   webSettings.setDefaultFontSize(11);
        //  tvDatas.loadDataWithBaseURL(null, finalData, "text/html", "UTF-8", null);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        // Setup Dialog

    //    mSubmitButton.setOnClickListener(this);
     //   implementSwipeDismiss();

       /* if (userSessionPrefs.isUserLogin()) {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            finish();
        }*/
    }
    @OnClick(R.id.activity_login_btn_login)
    void onLoginClicked(View v){
        if (TextUtils.isEmpty(mUserName.getText())) {
            mUserName.setError("Username Required");
            mUserName.requestFocus();
        } else if (TextUtils.isEmpty(mPassword.getText())) {
            mPassword.setError("Password Required");
            mPassword.requestFocus();
        } else {
            mPresenter.loginUser(this, mUserName.getText().toString(), mPassword.getText().toString());
        }
    }

    /*private void implementSwipeDismiss() {
        SwipeDismissBehavior swipeDismissBehavior = new SwipeDismissBehavior();
        swipeDismissBehavior.setSwipeDirection(SwipeDismissBehavior.SWIPE_DIRECTION_ANY);//Swipe direction i.e any direction, here you can put any direction LEFT or RIGHT
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) cvLogin.getLayoutParams();
        layoutParams.setBehavior(swipeDismissBehavior);//set swipe behaviour to Coordinator layout
        swipeDismissBehavior.setListener(new SwipeDismissBehavior.OnDismissListener() {
            @Override
            public void onDismiss(View view)(MainApp)getApplicationContext().get {
                lnLoginMain.setVisibility(View.VISIBLE);
                coLogin.setVisibility(View.GONE);
            }

            @Override
            public void onDragStateChanged(int i) {
            }
        });
    }*/

    private void initializeDependencies() {
        // Let's get the modules and inject them.
        MainApp mainApp=(MainApp) getApplicationContext();
//        loginComponent = DaggerApplicationComponent.builder()
        loginComponent=DaggerLoginComponent
                .builder()
                .applicationComponent(mainApp.getApplicationComponent())

                .build();

        loginComponent.inject(this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void hideLoading() {
        if(mDialog!=null) {
            mDialog.dismiss();
        }
    }

    @Override
    public void showMessage(int resId) {

    }


//    @Override
//    public void showSuccess() {
//        showError("Success");
//    }
//
//    @Override
//    public void showFailure(String message) {
//        showError(message);
//    }
//
//    @Override
//    public void setTransition() {
//
//    }

//    @Override
//    public void showDialog(String s) {
//        if (mDialog != null) {
//            mDialog.show();
//        }
//    }


    @Override
    public void onPause() {

        super.onPause();
        if (mDialog != null)
            mDialog.dismiss();
    }

    @Override
    public void initialize() {

    }


    @Override
    public void openMainActvity() {
        this.startActivity(new Intent(this, MainActivity.class));
        this.finish();
    }
}