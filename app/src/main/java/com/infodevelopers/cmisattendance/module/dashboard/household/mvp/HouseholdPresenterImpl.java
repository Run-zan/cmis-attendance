package com.infodevelopers.cmisattendance.module.dashboard.household.mvp;

import android.util.Log;

import com.infodevelopers.cmisattendance.base.presenter.BasePresenter;
import com.infodevelopers.cmisattendance.data.local.model.Child;
import com.infodevelopers.cmisattendance.data.local.model.ChildResponse;
import com.infodevelopers.cmisattendance.data.local.model.District;
import com.infodevelopers.cmisattendance.data.local.model.Household;
import com.infodevelopers.cmisattendance.data.local.model.VDC;
import com.infodevelopers.cmisattendance.data.local.model.Ward;
import com.infodevelopers.cmisattendance.data.repository.DataRepository;
import com.infodevelopers.cmisattendance.utils.SchedulerProvider;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.CompletableObserver;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subscribers.DisposableSubscriber;

public class HouseholdPresenterImpl<V extends HouseholdView> extends BasePresenter<V> implements HouseholdPresenter<V> {

    @Inject
    public HouseholdPresenterImpl(SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable, DataRepository dataRepository) {
        super(schedulerProvider, compositeDisposable, dataRepository);

    }

    @Override
    public List<District> getDistrict() {

        getCompositeDisposable().add(getDataRepository().getAllDistrict()
                .subscribeOn(
                        getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSubscriber<List<District>>() {
                    @Override
                    public void onNext(List<District> districtList) {
                        getMvpView().showDistrict(districtList);

                    }

                    @Override
                    public void onError(Throwable t) {
                        Log.d("error",t.toString());

                    }

                    @Override
                    public void onComplete() {

                    }
                }));

        return null;
    }

    @Override
    public List<VDC> getVdcForDistrict(int district_id) {
        getCompositeDisposable().add(getDataRepository().getVdcOfDistrict(district_id)
                .observeOn(getSchedulerProvider().ui())
                .subscribeOn(getSchedulerProvider().io())
                .subscribeWith(new DisposableSubscriber<List<VDC>>() {
                    @Override
                    public void onNext(List<VDC> vdcs) {
                        getMvpView().showVdc(vdcs);

                    }

                    @Override
                    public void onError(Throwable t) {

                    }

                    @Override
                    public void onComplete() {

                    }
                }));
        return null;
    }

    @Override
    public List<Ward> getWardForVdc(int vdc_id) {
        getCompositeDisposable().add(getDataRepository().getWardOfVdc(vdc_id)
                .observeOn(getSchedulerProvider().ui())
                .subscribeOn(getSchedulerProvider().io())
                .subscribeWith(new DisposableSubscriber<List<Ward>>() {
                    @Override
                    public void onNext(List<Ward> wardList) {

                        getMvpView().showWard(wardList);
                    }

                    @Override
                    public void onError(Throwable t) {

                    }

                    @Override
                    public void onComplete() {

                    }
                }));


        return null;
    }

    @Override
    public void fetchChildData(int districtId, int vdcId, int wardId) {
        getMvpView().showLoading("Downloading Household data..");
        getDataRepository().downloadChildData(districtId, vdcId, wardId)
                .subscribeOn(getSchedulerProvider().io())
                .flatMapCompletable(childResponse ->
                {
                    getMvpView().hideLoading();
                    if (!childResponse.getStatus().equals("success")) return null;

                    ChildResponse.MasterData masterData=childResponse.getData().getmMasterData();
                    List<Child> dataChildList = new ArrayList<>();
                    List<Child> childList = childResponse.getData().getChildren();
                    for (Child child : childList) {
                        child.setWard_name(masterData.getWard());
                        child.setDistrictId(masterData.getDistrict());
                        child.setVdcId(masterData.getVdc());
                        dataChildList.add(child);
                    }
                    return getDataRepository().insertChildData(dataChildList);

                })
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onComplete() {
                        getMvpView().showMessage("Succesfully downloaded Household Data");
                    }

                    @Override
                    public void onError(Throwable e) {

                        getMvpView().showMessage("Error");
                    }
                });


    }
}
