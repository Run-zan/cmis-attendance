package com.infodevelopers.cmisattendance.module.attendance.attendance_fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import com.infodevelopers.cmisattendance.R;
import com.infodevelopers.cmisattendance.base.BaseFragment;
import com.infodevelopers.cmisattendance.data.local.model.ChildAttendance;
import com.infodevelopers.cmisattendance.module.attendance.AttendanceActivity;
import com.infodevelopers.cmisattendance.module.attendance.adapter.AttendanceAdapter;
import com.infodevelopers.cmisattendance.module.attendance.di.AttendanceComponent;
import com.infodevelopers.cmisattendance.module.attendance.pojo.ExpectedData;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;


public class TakeAttendanceFragment extends BaseFragment implements TakeAttendanceView, AdapterView.OnItemSelectedListener {

    public static final String TAG =TakeAttendanceFragment.class.getSimpleName() ;
    private static final String KEY_DATA ="attendance_take_data" ;

    @Inject
    TakeAttendancePresenter<TakeAttendanceView> mPresenter;
    AttendanceComponent attendanceComponent;
    AttendanceAdapter attendanceAdapter;

    @BindView(R.id.fragment_attendance_spinner)
    Spinner mSelectDaySpnr;

    @BindView(R.id.fragment_attendance_btn)
    Button mSaveBtn;

    @BindView(R.id.fragment_attendance_rv)
    RecyclerView mRecyclerView;

    ExpectedData expectedData;
    private List<Integer> attendanceTakenDate = new ArrayList<>();
    int selectedDay = 1;
    private List<Integer> day_list = new ArrayList<>();
    private ArrayList<ChildAttendance> childAttendanceArrayList = new ArrayList<>();


    public static TakeAttendanceFragment newInstance(ExpectedData expectedData) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_DATA, expectedData);
        TakeAttendanceFragment fragment = new TakeAttendanceFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_attendance, container, false);
        ButterKnife.bind(this, view);
        prepareChildAttendanceData();
        initializeDependency(view);

        Bundle b = getArguments();
        expectedData = (ExpectedData)b.getParcelable(KEY_DATA);
        String district_id = expectedData.getDistrict_id();
        String vdc_id = expectedData.getVdc_id();
        String ward_name = expectedData.getWard_name();
        expectedData.getActivity_id();

        mPresenter.getChildData(district_id, vdc_id, ward_name);

        //mPresenter.findTotalDays(expectedData.getStart_date(), expectedData.getEnd_date());

         day_list = mPresenter.findTotalDays(expectedData.getStart_date(), expectedData.getEnd_date());

        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, day_list);
        mSelectDaySpnr.setAdapter(adapter);

        //to get the selected day
        mSelectDaySpnr.setOnItemSelectedListener(this);

        mSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    attendanceTakenDate.add(selectedDay);

            }
        });

        return view;
    }


    private void initializeDependency(View v) {
        AttendanceActivity attendanceActivity = (AttendanceActivity) getActivity();
        attendanceComponent = attendanceActivity.getAttendanceComponent();
        if (attendanceComponent != null) {
            attendanceComponent.inject(this);
            mPresenter.onAttach(this);
            setUnBinder(ButterKnife.bind(this, v));

        }
    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void setAdapter(List<ChildAttendance> households) {
        attendanceAdapter = new AttendanceAdapter(getActivity(), childAttendanceArrayList, new AttendanceAdapter.OnItemCheckListener() {

            @Override
            public void onItemCheck(ChildAttendance child, CheckBox checkBox, String day) {

                if (attendanceTakenDate.contains(Integer.parseInt(day))) {
                    Log.d("datas", attendanceTakenDate.toString());
                    ArrayList<String> presentList = child.getPresent_list();
                    if (presentList.contains(selectedDay)) {
                        checkBox.setChecked(true);
                    } else {
                        checkBox.setChecked(false);
                    }
                }
            }

            @Override
            public void onItemUncheck(ChildAttendance child, CheckBox checkBox, String day) {


            }
        });

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(attendanceAdapter);
    }


    @Override
    public void openMainActivity() {

    }

    @Override
    public void initialize() {

    }

    void prepareChildAttendanceData(){
        ArrayList<String> present=new ArrayList<>();
        ChildAttendance one=new ChildAttendance();
        one.setAttendance_id(1);
        one.setChild_id("99");
        one.setChildName("ram");
        one.setPresent_list(present);
        childAttendanceArrayList.add(one);


        ArrayList<String> present1=new ArrayList<>();
        ChildAttendance two=new ChildAttendance();
        two.setAttendance_id(1);
        two.setChild_id("93");
        two.setChildName("shyam");
        two.setPresent_list(present1);
        childAttendanceArrayList.add(two);


        ArrayList<String> present2=new ArrayList<>();
        ChildAttendance three=new ChildAttendance();
        three.setAttendance_id(1);
        three.setChild_id("92");
        three.setChildName("shyam");
        three.setPresent_list(present2);
        childAttendanceArrayList.add(three);

    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
         selectedDay = i+1;

         attendanceAdapter.setDay(String.valueOf(selectedDay));

        mRecyclerView.setAdapter(null);
        mRecyclerView.setLayoutManager(null);
        mRecyclerView.setAdapter(attendanceAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        attendanceAdapter.notifyDataSetChanged();

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {


    }
}
