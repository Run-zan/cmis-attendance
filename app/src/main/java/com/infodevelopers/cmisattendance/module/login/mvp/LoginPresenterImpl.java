package com.infodevelopers.cmisattendance.module.login.mvp;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.infodevelopers.cmisattendance.base.ConnectivityReceiver;
import com.infodevelopers.cmisattendance.base.presenter.BasePresenter;
import com.infodevelopers.cmisattendance.data.repository.DataRepository;
import com.infodevelopers.cmisattendance.di.scope.PerActivity;
import com.infodevelopers.cmisattendance.module.login.di.interactor.LoginInteractor;
import com.infodevelopers.cmisattendance.module.login.pojo.LoginResponse;
import com.infodevelopers.cmisattendance.network.ApiInterface;
import com.infodevelopers.cmisattendance.utils.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
@PerActivity
public class LoginPresenterImpl<V extends LoginView> extends BasePresenter<V> implements LoginPresenter<V>  {


    CompositeDisposable disposable;
    private SchedulerProvider schedulerProvider;

    @Inject
    LoginPresenterImpl(SchedulerProvider schedulerProvider,
                       CompositeDisposable compositeDisposable, DataRepository dataRepository) {
        super(schedulerProvider,compositeDisposable,dataRepository);
        initialize();
    }


    private void initialize() {
        if (disposable == null) {
            disposable = new CompositeDisposable();
        }
    }


    @Override
    public void loginUser(Context c, String username, String password) {
        getMvpView().showLoading("Processing,Please Wait...");

        if (ConnectivityReceiver.isConnected()) {
            getMvpView().showLoading("Processing,Please Wait...");

            disposable.add(getDataRepository().loginUser(username, password)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(data -> {
                        if (data.getStatus_code().equalsIgnoreCase("0")) {
                            //view.showSuccess();
                            getMvpView().hideLoading();
                            getMvpView().showMessage("success login");
                            getMvpView().openMainActvity();
                        } else {
//                            view.setTransition();
                            getMvpView().hideLoading();
                            getMvpView().onError(data.getMessage());
                        }
                    }, throwable -> {
//                        view.setTransition();
                        getMvpView().hideLoading();
                        getMvpView().onError("Login Error");

                    }));

        } else {
            getMvpView().hideLoading();
            getMvpView().onError("Please check your connection status");
        }
    }

    @Override
    public void subscribe() {
        if (disposable == null) {
            disposable = new CompositeDisposable();
        }
    }

    @Override
    public void unSubscribe() {
        disposable.clear();
        if (disposable != null) {
            disposable.dispose();
        }

    }
}
