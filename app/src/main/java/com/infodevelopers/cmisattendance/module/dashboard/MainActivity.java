package com.infodevelopers.cmisattendance.module.dashboard;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.navigation.NavigationView;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;

import androidx.cardview.widget.CardView;
import android.view.View;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.infodevelopers.cmisattendance.R;
import com.infodevelopers.cmisattendance.app.MainApp;
import com.infodevelopers.cmisattendance.base.BaseActivity;
import com.infodevelopers.cmisattendance.module.attendance.AttendanceActivity;
import com.infodevelopers.cmisattendance.module.dashboard.download.di.DaggerDownloadComponent;
import com.infodevelopers.cmisattendance.module.dashboard.download.di.DownloadComponent;
import com.infodevelopers.cmisattendance.module.dashboard.download.di.DownloadModule;
import com.infodevelopers.cmisattendance.module.dashboard.download.mvp.DownloadMVP;
import com.infodevelopers.cmisattendance.module.dashboard.household.HouseholdDialogFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity
        implements DownloadMVP.View, View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {


    CoordinatorLayout coordinatorLayout;
    @Inject
    DownloadMVP.DwnPresenter<DownloadMVP.View> presenter;
    DownloadComponent downloadComponent;

    @BindView(R.id.activity_main_cv_download)
    CardView mDownload;

    @BindView(R.id.activity_main_cv_upload)
    CardView mUpload;

    @BindView(R.id.activity_main_cv_attendance)
    CardView mAttendance;

    @BindView(R.id.activity_main_cv_edit)
    CardView mEditAttendance;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    public void onViewReady(Bundle savedInstances, Intent intent) {
        super.onViewReady(savedInstances, intent);

        initializeDependency();
        setUnBinder(ButterKnife.bind(this));
        setSupportActionBar(mToolbar);
        coordinatorLayout=findViewById(R.id.coordinator_main);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mDownload.setOnClickListener(this);
        mAttendance.setOnClickListener(this);
        presenter.onAttach(MainActivity.this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.activity_main_cv_download:
                 try{
                    presenter.startDownlaod();

                 }
                 catch (Exception e){

                 }
                break;
            case R.id.activity_main_cv_upload:
                 Toast.makeText(this, "Upload", Toast.LENGTH_SHORT).show();
                break;

            case R.id.activity_main_cv_attendance:
                    startActivity(new Intent(this, AttendanceActivity.class));
                break;

            case R.id.activity_main_cv_edit:
                 Toast.makeText(this, "Edit AttendanceMVP", Toast.LENGTH_SHORT).show();
                break;
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void initializeDependency(){
        MainApp mainApp=(MainApp) getApplicationContext();
        downloadComponent =DaggerDownloadComponent.builder()
                .applicationComponent(mainApp.getApplicationComponent())
                .downloadModule(new DownloadModule())
                .build();
        downloadComponent.inject(this);
    }

    //Handling download section of dashboard




    @Override
    public void updateMasterSize() {

    }

    @Override
    public void updateChildSize() {

    }

    @Override
    public void updateAdultSize() {

    }

    @Override
    public void showHouseHoldDialog() {
        HouseholdDialogFragment householdDialogFragment=new HouseholdDialogFragment();
        householdDialogFragment.show(getSupportFragmentManager(),"hello");
    }

    @Override
    public void initialize() {

    }

    @Override
    public void showMessage(int resId) {

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unSubscribe();
        presenter.onDetach();
    }

}
