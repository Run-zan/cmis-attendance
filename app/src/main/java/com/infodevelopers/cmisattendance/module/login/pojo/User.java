package com.infodevelopers.cmisattendance.module.login.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Serializable {

    @Expose
    @SerializedName("USERID")
    String USERID;
    @Expose
    @SerializedName("NAME")
    String NAME;
    @Expose
    @SerializedName("LOGIN_NAME")
    String LOGIN_NAME;

    public User(String USERID, String NAME, String LOGIN_NAME) {
        this.USERID = USERID;
        this.NAME = NAME;
        this.LOGIN_NAME = LOGIN_NAME;
    }

    public String getUSERID() {
        return USERID;
    }

    public String getNAME() {
        return NAME;
    }

    public String getLOGIN_NAME() {
        return LOGIN_NAME;
    }
}
