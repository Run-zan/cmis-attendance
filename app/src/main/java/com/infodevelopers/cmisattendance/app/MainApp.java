package com.infodevelopers.cmisattendance.app;

import android.app.Application;
import android.content.Context;
import com.facebook.stetho.Stetho;
import com.infodevelopers.cmisattendance.di.component.ApplicationComponent;
import com.infodevelopers.cmisattendance.di.component.DaggerApplicationComponent;
import com.infodevelopers.cmisattendance.di.module.ApiModule;
import com.infodevelopers.cmisattendance.di.module.ApplicationModule;

public class MainApp extends Application {
    private static MainApp mInstance;
    ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        Stetho.initializeWithDefaults(this);

        initializeDependancies();
        mInstance= this;
    }

    public static MainApp get(Context context){
        return (MainApp) context.getApplicationContext();
    }

    private void initializeDependancies(){
            applicationComponent = DaggerApplicationComponent.builder().
                    applicationModule(new ApplicationModule(getApplicationContext())).
                    apiModule(new ApiModule()).
                    build();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static synchronized MainApp getmInstance(){
        return mInstance;
    }

    public ApplicationComponent getApplicationComponent(){
        return applicationComponent;
    }
}
