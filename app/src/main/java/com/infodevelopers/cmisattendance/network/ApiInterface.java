package com.infodevelopers.cmisattendance.network;

import com.infodevelopers.cmisattendance.data.local.model.ActivityResponse;
import com.infodevelopers.cmisattendance.data.local.model.ChildResponse;
import com.infodevelopers.cmisattendance.data.local.model.DistrictResponse;
import com.infodevelopers.cmisattendance.data.local.model.VDC;
import com.infodevelopers.cmisattendance.data.local.model.VdcResponse;
import com.infodevelopers.cmisattendance.data.local.model.WardResponse;
import com.infodevelopers.cmisattendance.module.login.pojo.LoginResponse;

import java.util.List;


import javax.inject.Singleton;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Url;

@Singleton
public interface ApiInterface {
    @FormUrlEncoded
    @POST("login")
    Observable<LoginResponse> login(@Field("USERNAME") String username,
                                                 @Field("USERTYPE") String usertype,
                                                 @Field("SIGNATURE") String signature);

    @FormUrlEncoded
    @POST
    Observable<Object> dataDownload(@Url String url , @Field("token") String token,
                                    @Field("type") String type);
    @FormUrlEncoded
    @POST
   Observable<VdcResponse> getVdcList(@Url String url, @Field("token")String token, @Field("type")String type );

    @FormUrlEncoded
    @POST
   Observable<WardResponse> getWardList(@Url String url, @Field("token")String token, @Field("type")String type );

    @FormUrlEncoded
    @POST
    Observable<DistrictResponse> getDistrictResponse(@Url String url, @Field("token")String token, @Field("type")String type );

    @FormUrlEncoded
    @POST
    Observable<ChildResponse> getChildResponse(@Url String url,@Field("token")String token,
                                               @Field("district")int district,@Field("vdc")int vdc,
                                               @Field("ward")int ward );

    @FormUrlEncoded
    @POST
    Observable<ActivityResponse> getActivityResponse(@Url String url,@Field("token")String token,@Field("type")String type);
//    @FormUrlEncoded
//    @POST
//    Observable<SchoolResponse> getSchoolList(@Url String url, @Field("token")String token, @Field("type")String type );

}

