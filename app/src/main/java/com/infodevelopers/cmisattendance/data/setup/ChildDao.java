package com.infodevelopers.cmisattendance.data.setup;

import com.infodevelopers.cmisattendance.data.local.model.Child;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Index;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import io.reactivex.Completable;
import io.reactivex.Flowable;

@Dao
public interface ChildDao {
    @Insert( onConflict = OnConflictStrategy.REPLACE)
    Completable insertChild(List<Child> childList);

    @Query("Select * from child WHERE ward_name=:ward_name AND district_id=:district_id AND vdc_id=:vdc_id ")
    Flowable<List<Child>> getChildForWardId(String ward_name,String district_id,String vdc_id);

}
