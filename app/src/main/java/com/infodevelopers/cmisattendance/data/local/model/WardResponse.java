package com.infodevelopers.cmisattendance.data.local.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WardResponse {
    @SerializedName("data")
private List<Ward> mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("status")
    private String mStatus;

    public List<Ward> getData() {
        return mData;
    }

    public void setData(List<Ward> data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }


}
