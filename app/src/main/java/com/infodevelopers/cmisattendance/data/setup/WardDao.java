package com.infodevelopers.cmisattendance.data.setup;

import com.infodevelopers.cmisattendance.data.local.model.Ward;
import com.infodevelopers.cmisattendance.module.attendance.pojo.DownloadedWard;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import io.reactivex.Completable;
import io.reactivex.Flowable;

@Dao
public interface WardDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable putWard(List<Ward> wardResponseList);

    @Query("SELECT * FROM ward")
    Flowable<List<Ward>> getAll();

    @Query("SELECT * FROM ward WHERE vdc_id IN (:ward)")
    Flowable<List<Ward>> loadAllByIds(int ward);

    @Query("SELECT *,CASE WHEN EXISTS(SELECT * FROM child WHERE child.ward_name=ward.ward_id AND child.vdc_id=ward.vdc_id)  then 1 else 0 end as downloaded from ward ")
    Flowable<List<Ward>> getWardDownloadInfo();
}
