package com.infodevelopers.cmisattendance.data.local.model;




import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ActivityResponse {
    @SerializedName("status")
    String status;
    @SerializedName("message")
    String message;

    @SerializedName("data")
    List<ActivityModel> activityModelList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ActivityModel> getData() {
        return activityModelList;
    }

    public void setData(List<ActivityModel> activityModelList) {
        this.activityModelList = activityModelList;
    }
}
