
package com.infodevelopers.cmisattendance.data.local.model;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;


@Generated("net.hexar.json2pojo")
@Entity(indices ={@Index(value = {"vdc_id","ward_id"},unique = true)})
public class Ward {

@PrimaryKey(autoGenerate = true)
 private int id;

        @SerializedName("DISTRICT_ID")
        @ColumnInfo(name = "district_id")
        private String mDISTRICTID;

        @SerializedName("VDC_ID")
        @ColumnInfo(name = "vdc_id")
        private String mVDCID;

        @SerializedName("WARD_ID")
        @ColumnInfo(name = "ward_id")
        private String mWARDID;

        @ColumnInfo(name = "downloaded")
        private Boolean downloaded;

    public Boolean getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(Boolean downloaded) {
        this.downloaded = downloaded;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDISTRICTID() {
            return mDISTRICTID;
        }

        public void setDISTRICTID(String dISTRICTID) {
            mDISTRICTID = dISTRICTID;
        }

        public String getVDCID() {
            return mVDCID;
        }

        public void setVDCID(String vDCID) {
            mVDCID = vDCID;
        }

        public String getWARDID() {
            return mWARDID;
        }

        public void setWARDID(String wARDID) {
            mWARDID = wARDID;
        }

    @NonNull
    @Override
    public String toString() {
        return mWARDID;
    }
}
