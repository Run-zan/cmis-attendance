package com.infodevelopers.cmisattendance.data.local.model;



public class DownloadWrapper {
    private WardResponse wardResponses;
    private VdcResponse vdcResponses;
    private DistrictResponse districtResponse;
    private ActivityResponse activityResponse;

    public DownloadWrapper(WardResponse wardResponses, VdcResponse vdcResponses, DistrictResponse districtResponse, ActivityResponse activityResponse) {
        this.wardResponses = wardResponses;
        this.vdcResponses = vdcResponses;
        this.districtResponse = districtResponse;
        this.activityResponse = activityResponse;
    }

    public ActivityResponse getActivityResponse() {
        return activityResponse;
    }

    public WardResponse getWardResponses() {
        return wardResponses;
    }

    public VdcResponse getVdcResponses() {
        return vdcResponses;
    }

    public DistrictResponse getDistrictResponse() {
        return districtResponse;
    }
}
