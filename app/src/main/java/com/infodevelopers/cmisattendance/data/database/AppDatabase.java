package com.infodevelopers.cmisattendance.data.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.infodevelopers.cmisattendance.data.local.model.ActivityModel;
import com.infodevelopers.cmisattendance.data.local.model.Attendance;
import com.infodevelopers.cmisattendance.data.local.model.Child;
import com.infodevelopers.cmisattendance.data.local.model.ChildAttendance;
import com.infodevelopers.cmisattendance.data.local.model.District;
import com.infodevelopers.cmisattendance.data.local.model.VDC;
import com.infodevelopers.cmisattendance.data.local.model.Ward;
import com.infodevelopers.cmisattendance.data.setup.ActivityDao;
import com.infodevelopers.cmisattendance.data.setup.AttendanceDao;
import com.infodevelopers.cmisattendance.data.setup.ChildAttendanceDao;
import com.infodevelopers.cmisattendance.data.setup.ChildDao;
import com.infodevelopers.cmisattendance.data.setup.Converters;
import com.infodevelopers.cmisattendance.data.setup.DistrictDao;
import com.infodevelopers.cmisattendance.data.setup.VDCDao;
import com.infodevelopers.cmisattendance.data.setup.WardDao;



@Database(entities = {VDC.class, Ward.class, District.class,Child.class,ActivityModel.class,ChildAttendance.class,Attendance.class}, version = 1)
@TypeConverters({Converters.class})

public abstract class AppDatabase extends RoomDatabase {
    public abstract VDCDao vdcDao();
    public abstract WardDao wardDao();
    public abstract DistrictDao districtDao();
    public abstract ActivityDao activityDao();
    public abstract ChildDao childDao();
    public abstract AttendanceDao attendanceDao();
    public abstract ChildAttendanceDao studentAttendanceDao();
    // public abstract SchoolDao schoolDao();
}
