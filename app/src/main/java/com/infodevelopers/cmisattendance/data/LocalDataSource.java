package com.infodevelopers.cmisattendance.data;

import com.infodevelopers.cmisattendance.data.local.model.ActivityModel;
import com.infodevelopers.cmisattendance.data.local.model.Attendance;
import com.infodevelopers.cmisattendance.data.local.model.Child;
import com.infodevelopers.cmisattendance.data.local.model.District;
import com.infodevelopers.cmisattendance.data.local.model.ChildAttendance;
import com.infodevelopers.cmisattendance.data.local.model.VDC;
import com.infodevelopers.cmisattendance.data.local.model.Ward;
import com.infodevelopers.cmisattendance.data.setup.ActivityDao;
import com.infodevelopers.cmisattendance.data.setup.AttendanceDao;
import com.infodevelopers.cmisattendance.data.setup.ChildDao;
import com.infodevelopers.cmisattendance.data.setup.DistrictDao;
import com.infodevelopers.cmisattendance.data.setup.ChildAttendanceDao;
import com.infodevelopers.cmisattendance.data.setup.VDCDao;
import com.infodevelopers.cmisattendance.data.setup.WardDao;

import java.util.List;
import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;

public class LocalDataSource  {

    private VDCDao vdcDao;
    private WardDao wardDao;
    private DistrictDao districtDao;
    private ChildDao childDao;
    private ActivityDao activityDao;
    private AttendanceDao attendanceDao;
    private ChildAttendanceDao studentAttendanceDao;



    @Inject
    public LocalDataSource(VDCDao vdcDao,WardDao wardDao,DistrictDao toleDao,ChildDao childDao,ActivityDao activityDao,AttendanceDao attendanceDao
    ,ChildAttendanceDao studentAttendanceDao) {
        this.vdcDao = vdcDao;
        this.wardDao=wardDao;
        this.districtDao=toleDao;
        this.childDao=childDao;
        this.activityDao=activityDao;
        this.attendanceDao=attendanceDao;
        this.studentAttendanceDao=studentAttendanceDao;
    }


    public Completable putVdc(List<VDC> vdcs) {
       return vdcDao.insert(vdcs);

    }


    public Completable putWard(List<Ward> wardResponses) {
    return    wardDao.putWard(wardResponses);

    }


    public Completable putDistrict(List<District> districtList) {
       return districtDao.insert(districtList);

    }


    public Flowable<List<Ward>> getAllWard() {
        return null;
    }


    public Flowable<List<District>> getAllDistrict() {
        return districtDao.getAll();
    }

    public Flowable<List<VDC>> getAllVdc() {
        return null;
    }

    public Flowable<List<VDC>> getVdcOfDistrict(int districtId) {
        return vdcDao.loadAllByIds(districtId);
    }

    public Flowable<List<Ward>> getWardOfVdc(int vdcId) {
        return wardDao.loadAllByIds(vdcId);
    }

    public Completable insertChildData(List<Child> childList) {
        return childDao.insertChild(childList);
    }

//    public Flowable<List<Child>> getChildData(int ward_id) {
//        return childDao.getChildForWardId(ward_id);
//    }

    public Completable putActivity(List<ActivityModel> data) {
       return activityDao.insertActivity(data);

    }

    public Flowable<List<ActivityModel>> getAllActivity() {
        return activityDao.getAllActivity();
    }

    public Flowable<List<Child>> getChildData(String district_id, String vdc_id, String ward_id) {
        return childDao.getChildForWardId(ward_id,district_id,vdc_id);
    }

    public Flowable<List<Ward>> getDownloadedWardInfo() {
        return wardDao.getWardDownloadInfo();

    }

    public Completable insertAttendance(Attendance attendance) {
      return   attendanceDao.putAttendance(attendance);
    }

    public Flowable<List<Attendance>> getAllAttendance() {
        return attendanceDao.getAllAttendance();
    }

    public Completable insertStudentAttendance(List<ChildAttendance> childAttendance) {
        return studentAttendanceDao.putStudentAttendance(childAttendance);
    }

    public Flowable<List<ChildAttendance>> getStudentAttendance(int activity_id) {
        return studentAttendanceDao.getStudentAttendanceById(activity_id);
    }
}
