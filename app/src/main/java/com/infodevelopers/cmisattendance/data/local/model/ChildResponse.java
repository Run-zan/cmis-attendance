package com.infodevelopers.cmisattendance.data.local.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChildResponse {
    @SerializedName("data")
    private ChildData mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("status")
    private String mStatus;


    public ChildData getData() {
        return mData;
    }

    public void setData(ChildData data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public class MasterData{
        @SerializedName("district")
        String district;

        @SerializedName("vdc")
        String vdc;

        @SerializedName("ward")
        String ward;

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getVdc() {
            return vdc;
        }

        public void setVdc(String vdc) {
            this.vdc = vdc;
        }

        public String getWard() {
            return ward;
        }

        public void setWard(String ward) {
            this.ward = ward;
        }
    }

    public class ChildData{
        @SerializedName("child_data")
        List<Child> children;
        @SerializedName("master_data")
        private MasterData mMasterData;

        public MasterData getmMasterData() {
            return mMasterData;
        }

        public void setmMasterData(MasterData mMasterData) {
            this.mMasterData = mMasterData;
        }

        public List<Child> getChildren() {
            return children;
        }

        public void setChildren(List<Child> children) {
            this.children = children;
        }
    }
}
