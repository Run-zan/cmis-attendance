package com.infodevelopers.cmisattendance.data.repository;

import com.infodevelopers.cmisattendance.data.LocalDataSource;
import com.infodevelopers.cmisattendance.data.RemoteDataSource;
import com.infodevelopers.cmisattendance.data.local.model.ActivityModel;
import com.infodevelopers.cmisattendance.data.local.model.Attendance;
import com.infodevelopers.cmisattendance.data.local.model.Child;
import com.infodevelopers.cmisattendance.data.local.model.ChildAttendance;
import com.infodevelopers.cmisattendance.data.local.model.ChildResponse;
import com.infodevelopers.cmisattendance.data.local.model.District;
import com.infodevelopers.cmisattendance.data.local.model.DownloadWrapper;
import com.infodevelopers.cmisattendance.data.local.model.VDC;
import com.infodevelopers.cmisattendance.data.local.model.Ward;
import com.infodevelopers.cmisattendance.module.login.pojo.LoginResponse;
import com.infodevelopers.cmisattendance.utils.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;

public class DataRepository implements DataInterface {

    private LocalDataSource localDataSource;
    private RemoteDataSource remoteDataSource;
    private SchedulerProvider schedulerProvider;

    @Inject
    public DataRepository(LocalDataSource localDataSource, RemoteDataSource remoteDataSource, SchedulerProvider schedulerProvider) {
        this.localDataSource = localDataSource;
        this.remoteDataSource = remoteDataSource;
        this.schedulerProvider = schedulerProvider;
    }


    @Override
    public Observable<LoginResponse> loginUser(String username, String password) {
        return remoteDataSource.loginOnline(username, password);
    }

//    @Override
//    public ObservableSource<Boolean> putVdc(List<VDC> vdcs) {
//
//        return localDataSource.putVdc(vdcs);
//    }
//
//    @Override
//    public ObservableSource<Boolean> putWard(List<Ward> wardResponses) {
//        return localDataSource.putWard(wardResponses);
//    }
//
//    @Override
//    public ObservableSource<Boolean> putDistrict(List<District> toleResponses) {
//        return localDataSource.putDistrict(toleResponses);
//    }

    @Override
    public Flowable<List<Ward>> getAllWard() {
        return localDataSource.getAllWard();
    }

    @Override
    public Flowable<List<District>> getAllDistrict() {
        return localDataSource.getAllDistrict();
    }

    @Override
    public Completable storeDownloadData(DownloadWrapper downloadWrapper) {
        return localDataSource.putVdc(downloadWrapper.getVdcResponses().getData())
                .andThen(localDataSource.putDistrict(downloadWrapper.getDistrictResponse().getData()))
                .andThen(localDataSource.putWard(downloadWrapper.getWardResponses().getData()))
                .andThen(localDataSource.putActivity(downloadWrapper.getActivityResponse().getData()));
    }


    @Override
    public Flowable<List<VDC>> getAllVdc() {

        return localDataSource.getAllVdc();
    }

    @Override
    public Observable<DownloadWrapper> getDownloadData() {
        Observable<DownloadWrapper> downloadWrapper = remoteDataSource.getDownloadData();
        return downloadWrapper;
    }

    @Override
    public Flowable<List<VDC>> getVdcOfDistrict(int districtId) {
        return localDataSource.getVdcOfDistrict(districtId);
    }

    @Override
    public Flowable<List<Ward>> getWardOfVdc(int vdcId) {
        return localDataSource.getWardOfVdc(vdcId);
    }

    @Override
    public Observable<ChildResponse> downloadChildData(int district, int vdc, int ward) {
        return remoteDataSource.downloadChildData(district, vdc, ward);
    }

    @Override
    public Completable insertChildData(List<Child> childList) {
        return localDataSource.insertChildData(childList);
    }

    @Override
    public Flowable<List<Child>> getChildData(String district_id, String vdc_id, String ward_id) {
        return localDataSource.getChildData(district_id,vdc_id,ward_id);
    }



    @Override
    public Flowable<List<ActivityModel>> getAllActivity() {
        return localDataSource.getAllActivity();
    }

    @Override
    public Flowable<List<Ward>> getDownloadedWardInfo() {
        return localDataSource.getDownloadedWardInfo();
    }

    @Override
    public Completable insertAttendance(Attendance attendance) {

        return localDataSource.insertAttendance(attendance);
    }

    @Override
    public Flowable<List<Attendance>> getAllAttendance() {

        return localDataSource.getAllAttendance();
    }

    @Override
    public Completable insertStudentAttendance(List<ChildAttendance> childAttendance) {

        return localDataSource.insertStudentAttendance(childAttendance);
    }

    @Override
    public Flowable<List<ChildAttendance>> getStudentAttendance(int activity_id) {

        return localDataSource.getStudentAttendance(activity_id);
    }


}
