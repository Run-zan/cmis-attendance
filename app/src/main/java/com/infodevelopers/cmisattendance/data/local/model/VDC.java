
package com.infodevelopers.cmisattendance.data.local.model;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
@Entity(indices ={@Index(value = {"vdc_id","district_id"},unique = true)})
public class VDC {

        @ColumnInfo(name = "vdc_id")
        @PrimaryKey
        @NonNull
        @SerializedName("VDC_ID")
        private String mVDCID;

        @ColumnInfo(name = "vdc_name")
        @SerializedName("VDC_NAME")
        private String mVDCNAME;

        @ColumnInfo(name = "district_id")
        @SerializedName("DISTRICT_ID")
        private String districtId;

    @NonNull
    public String getmVDCID() {
        return mVDCID;
    }

    public void setmVDCID(@NonNull String mVDCID) {
        this.mVDCID = mVDCID;
    }

    public String getmVDCNAME() {
        return mVDCNAME;
    }

    public void setmVDCNAME(String mVDCNAME) {
        this.mVDCNAME = mVDCNAME;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getVDCID() {
            return mVDCID;
        }

        public void setVDCID(String vDCID) {
            mVDCID = vDCID;
        }

        public String getVDCNAME() {
            return mVDCNAME;
        }

        public void setVDCNAME(String vDCNAME) {
            mVDCNAME = vDCNAME;
        }

    @NonNull
    @Override
    public String toString() {
        return mVDCNAME;
    }
}
