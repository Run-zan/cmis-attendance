package com.infodevelopers.cmisattendance.data.di;

import androidx.room.Room;
import android.content.Context;


import com.infodevelopers.cmisattendance.data.database.AppDatabase;
import com.infodevelopers.cmisattendance.data.setup.ActivityDao;
import com.infodevelopers.cmisattendance.data.setup.AttendanceDao;
import com.infodevelopers.cmisattendance.data.setup.ChildAttendanceDao;
import com.infodevelopers.cmisattendance.data.setup.ChildDao;
import com.infodevelopers.cmisattendance.data.setup.DistrictDao;
import com.infodevelopers.cmisattendance.data.setup.VDCDao;
import com.infodevelopers.cmisattendance.data.setup.WardDao;
import com.infodevelopers.cmisattendance.utils.Constants;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {
    private static final String DATABASE = "database_name";

    @Provides
    @Named(DATABASE)
    String provideDatabaseName() {
        return Constants.DATABASE_NAME;
    }

    @Provides
    @Singleton
    AppDatabase provideStackOverflowDao( Context context,@Named(DATABASE) String databaseName) {
        return Room.databaseBuilder(context, AppDatabase.class, databaseName).build();
    }

    @Provides
    @Singleton
    DistrictDao provideDistrcit(AppDatabase appDatabase){
        return appDatabase.districtDao();
    }
    @Provides
    @Singleton
    WardDao provideWardDao(AppDatabase appDatabase){return
    appDatabase.wardDao();}

    @Provides
    @Singleton
    VDCDao provideVdcDao(AppDatabase appDatabase) {
        return appDatabase.vdcDao();
    }

    @Provides
    @Singleton
    ChildDao provideChildDao(AppDatabase appDatabase){
        return appDatabase.childDao();
    }


    @Provides
    @Singleton
    ActivityDao provideActivityDao(AppDatabase appDatabase){return appDatabase.activityDao();}

    @Provides
    @Singleton
    AttendanceDao provideAttendanceDao(AppDatabase appDatabase){return appDatabase.attendanceDao();}

    @Provides
    @Singleton
    ChildAttendanceDao provideStudentAttendanceDao(AppDatabase appDatabase){return appDatabase.studentAttendanceDao();};

}
