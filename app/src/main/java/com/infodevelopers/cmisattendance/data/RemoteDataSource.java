package com.infodevelopers.cmisattendance.data;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.infodevelopers.cmisattendance.base.UserSessionPrefs;
import com.infodevelopers.cmisattendance.data.local.model.ActivityResponse;
import com.infodevelopers.cmisattendance.data.local.model.ChildResponse;
import com.infodevelopers.cmisattendance.data.local.model.DistrictResponse;
import com.infodevelopers.cmisattendance.data.local.model.DownloadWrapper;
import com.infodevelopers.cmisattendance.data.local.model.VdcResponse;
import com.infodevelopers.cmisattendance.data.local.model.WardResponse;
import com.infodevelopers.cmisattendance.module.login.extras.SHA256;
import com.infodevelopers.cmisattendance.module.login.pojo.LoginResponse;
import com.infodevelopers.cmisattendance.network.ApiInterface;
import com.infodevelopers.cmisattendance.utils.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;


public class RemoteDataSource  {
    private ApiInterface apiService;
    private UserSessionPrefs userSessionPrefs;
    private LoginResponse loginResponse;
    private SchedulerProvider schedulerProvider;
    @Inject
    public RemoteDataSource(ApiInterface apiService,UserSessionPrefs userSessionPrefs,SchedulerProvider schedulerProvider) {
        this.apiService=apiService;
        this.userSessionPrefs=userSessionPrefs;
        this.schedulerProvider=schedulerProvider;
        getLoginResponse();
    }
    private void getLoginResponse(){
        loginResponse =  new Gson().fromJson(userSessionPrefs.getsaveLogin(), new TypeToken<LoginResponse>(){
        }.getType());
    }



    public Observable<LoginResponse> loginOnline( String username, String password) {

        return apiService.login(
                username,
                getUserType("Staff"),
                getEncrypted(getUserType("Staff") + username + password))
                .concatMap(loginResponse -> {
                    if (loginResponse.getStatus().equals("success")) {

                        userSessionPrefs.saveLogin(loginResponse);
                    }
                    return Observable.just(loginResponse);
                });

    }
    private String getEncrypted(String s) {
        String hash=SHA256.getEncryptedValue(s);
        Log.d("hash",hash);
        return hash;
    }


    private String getUserType(String s) {
        if (s.equalsIgnoreCase("Staff")) {
            return "1";
        } else {
            return "2";
        }
    }


    public Observable<DownloadWrapper> getDownloadData() {
        String token=loginResponse.getToken();
      Observable<DistrictResponse> districtObservable=  apiService.getDistrictResponse("http://192.168.40.25:8081/git/opmis_sponsorship/api/download",token, "district")
              .subscribeOn(schedulerProvider.io());
        Observable<VdcResponse> vdcObservable=  apiService.getVdcList("http://192.168.40.25:8081/git/opmis_sponsorship/api/download",token, "getAllVdc")
                  .subscribeOn(schedulerProvider.io());
        Observable<WardResponse> wardObservable=  apiService.getWardList("http://192.168.40.25:8081/git/opmis_sponsorship/api/download",token, "getAllWard")
                .subscribeOn(schedulerProvider.io());
        Observable<ActivityResponse> activityObservable=apiService.getActivityResponse("http://192.168.40.25:8081/git/opmis_sponsorship/api/download",token,"activity");

      return   Observable.zip(districtObservable,vdcObservable,wardObservable,activityObservable,(districtResponse, vdcResponse, wardResponse,activityResponse) -> {
            DownloadWrapper wrapper=new DownloadWrapper(wardResponse,vdcResponse,districtResponse,activityResponse);

       return wrapper; })
              .doOnError(new Consumer<Throwable>() {
                  @Override
                  public void accept(Throwable throwable) throws Exception {
                      return  ;
                  }
              });




    }

    public Observable<ChildResponse> downloadChildData(int district, int vdc, int ward) {
        String token=loginResponse.getToken();
return apiService.getChildResponse("http://192.168.40.25:8081/git/opmis_sponsorship/api/household",
        token,district,vdc,ward);
    }
}
