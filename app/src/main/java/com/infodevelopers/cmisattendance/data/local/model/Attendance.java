package com.infodevelopers.cmisattendance.data.local.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(indices = {@Index(value = {"activity_id","district_id","vdc_id","ward_name"},unique = true)})
public class Attendance {
    @ColumnInfo(name = "attendance_id")
    @PrimaryKey(autoGenerate = true)
    private int attendance_id;

    @ColumnInfo(name = "start_date")
    private String start_date;

    @ColumnInfo(name = "end_date")
    private String end_date;

    @ColumnInfo(name = "days")
    private int days;

    @ColumnInfo(name = "activity_id")
    private String activity_id;

    @ColumnInfo(name = "district_id")
    String district_id;

    @ColumnInfo(name = "vdc_id")
    String vdc_id;

    @ColumnInfo(name = "ward_name")
    String ward_name;

    public String getDistrict_id() {
        return district_id;
    }

    public void setDistrict_id(String district_id) {
        this.district_id = district_id;
    }

    public String getVdc_id() {
        return vdc_id;
    }

    public void setVdc_id(String vdc_id) {
        this.vdc_id = vdc_id;
    }

    public String getWard_name() {
        return ward_name;
    }

    public void setWard_name(String ward_name) {
        this.ward_name = ward_name;
    }

    public Attendance( String start_date, String end_date, int days, String activity_id, String district_id, String vdc_id, String ward_name) {
        this.start_date = start_date;
        this.end_date = end_date;
        this.days = days;
        this.activity_id = activity_id;
        this.district_id = district_id;
        this.vdc_id = vdc_id;
        this.ward_name = ward_name;
    }

    public int getAttendance_id() {
        return attendance_id;
    }

    public void setAttendance_id(int attendance_id) {
        this.attendance_id = attendance_id;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public String getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(String activity_id) {
        this.activity_id = activity_id;
    }
}
