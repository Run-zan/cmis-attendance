
package com.infodevelopers.cmisattendance.data.local.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Generated("net.hexar.json2pojo")
public class Household {

    @SerializedName("data")
    private Data mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("status")
    private String mStatus;

    public Data getData() {
        return mData;
    }

    public void setData(Data data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }


    public class Data {

        @SerializedName("child_data")
        private List<ChildDatum> mChildData;
        @SerializedName("master_data")
        private MasterData mMasterData;

        public List<ChildDatum> getChildData() {
            return mChildData;
        }

        public void setChildData(List<ChildDatum> childData) {
            mChildData = childData;
        }

        public MasterData getMasterData() {
            return mMasterData;
        }

        public void setMasterData(MasterData masterData) {
            mMasterData = masterData;
        }

        public class ChildDatum {

            @ColumnInfo(name = "child_id")
            @SerializedName("ID")
            private String mID;

            @ColumnInfo(name = "child_name")
            @SerializedName("NAME")
            private String mNAME;

            public String getID() {
                return mID;
            }

            public void setID(String iD) {
                mID = iD;
            }

            public String getNAME() {
                return mNAME;
            }

            public void setNAME(String nAME) {
                mNAME = nAME;
            }

        }

        public class MasterData {

            @SerializedName("district")
            private String mDistrict;

            @SerializedName("vdc")
            private String mVdc;

            @SerializedName("ward")
            private String mWard;

            public String getDistrict() {
                return mDistrict;
            }

            public void setDistrict(String district) {
                mDistrict = district;
            }

            public String getVdc() {
                return mVdc;
            }

            public void setVdc(String vdc) {
                mVdc = vdc;
            }

            public String getWard() {
                return mWard;
            }

            public void setWard(String ward) {
                mWard = ward;
            }

        }


    }
}
