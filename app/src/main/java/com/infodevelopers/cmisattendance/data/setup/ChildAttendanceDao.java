package com.infodevelopers.cmisattendance.data.setup;

import com.infodevelopers.cmisattendance.data.local.model.ChildAttendance;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import io.reactivex.Completable;
import io.reactivex.Flowable;

@Dao
public interface ChildAttendanceDao {

    @Insert
    Completable putStudentAttendance(List<ChildAttendance> childAttendances);

    @Query("Select * from ChildAttendance where attendance_id=:attendance_id")
    Flowable<List<ChildAttendance>> getStudentAttendanceById(int attendance_id);
//
//    @Query("SELECT * from childattendance,child,attendance where child.district_id=attendance.district_id AND child.ward_name=attendance.ward_name" +
//            " AND child.district_id=attendance.district_id")
//    Flowable<List<ChildAttendance>> getChildAttendance();
}
