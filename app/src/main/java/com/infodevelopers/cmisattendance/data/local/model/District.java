
package com.infodevelopers.cmisattendance.data.local.model;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.OnConflictStrategy;
import androidx.room.PrimaryKey;

@Generated("net.hexar.json2pojo")
@Entity
public class District implements Serializable {
        @PrimaryKey
        @ColumnInfo(name = "column_id")
        @NonNull
        @SerializedName("DISTRICT_ID")
        private String mDISTRICTID;

        @ColumnInfo(name = "column_name")
        @SerializedName("DISTRICT_NAME")
        private String mDISTRICTNAME;

        public String getDISTRICTID() {
            return mDISTRICTID;
        }

        public void setDISTRICTID(String dISTRICTID) {
            mDISTRICTID = dISTRICTID;
        }

        public String getDISTRICTNAME() {
            return mDISTRICTNAME;
        }

        public void setDISTRICTNAME(String dISTRICTNAME) {
            mDISTRICTNAME = dISTRICTNAME;
        }

    @NonNull
    @Override
    public String toString() {
        return mDISTRICTNAME;
    }
}
