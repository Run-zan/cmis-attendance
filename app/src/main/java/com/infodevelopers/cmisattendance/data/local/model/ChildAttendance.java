package com.infodevelopers.cmisattendance.data.local.model;

import java.util.ArrayList;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import static androidx.room.ForeignKey.CASCADE;


@Entity(foreignKeys = @ForeignKey(entity = Attendance.class,parentColumns = "attendance_id",childColumns =
        "attendance_id",onDelete =CASCADE ))
public class ChildAttendance {   @PrimaryKey
    @ColumnInfo(name = "id")
    int id;

    @ColumnInfo(name = "attendance_id")
    int attendance_id;

    @ColumnInfo(name = "child_name")
    String childName;

    @ColumnInfo(name = "child_id")
    String child_id;

    @ColumnInfo(name = "present_days")
    ArrayList<String> present_list=new ArrayList<>();

    public int getId() {
        return id;
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAttendance_id() {
        return attendance_id;
    }

    public void setAttendance_id(int attendance_id) {
        this.attendance_id = attendance_id;
    }

    public String getChild_id() {
        return child_id;
    }

    public void setChild_id(String child_id) {
        this.child_id = child_id;
    }

    public ArrayList<String> getPresent_list() {
        return present_list;
    }

    public void setPresent_list(ArrayList<String> present_list) {
        this.present_list = present_list;
    }

}


