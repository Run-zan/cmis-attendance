package com.infodevelopers.cmisattendance.data.local.model;

import com.google.gson.annotations.SerializedName;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity
public class Child {



    @SerializedName("ID")
    @ColumnInfo(name = "child_id")
    @PrimaryKey
    private int child_id;

    @SerializedName("NAME")
    @ColumnInfo(name = "child_name")
    private String name;

    @ColumnInfo(name = "ward_name")
    private String ward_name;

    @ColumnInfo(name = "vdc_id")
    private String vdcId;

    @ColumnInfo(name = "district_id")
    private String districtId;

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getVdcId() {
        return vdcId;
    }

    public void setVdcId(String vdcId) {
        this.vdcId = vdcId;
    }

    public String getWard_name() {
        return ward_name;
    }

    public void setWard_name(String ward_name) {
        this.ward_name = ward_name;
    }

    public int getChild_id() {
        return child_id;
    }

    public void setChild_id(int child_id) {
        this.child_id = child_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
