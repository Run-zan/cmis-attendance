package com.infodevelopers.cmisattendance.data.local.model;


import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ActivityModel {
    @SerializedName("ACTIVITY_NAME")
    @ColumnInfo(name = "activity_name")
    String activity_name;

    @SerializedName("ACTIVITY_ID")
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "activity_id")
    String activity_id;

    public String getActivity_name() {
        return activity_name;
    }

    public void setActivity_name(String activity_name) {
        this.activity_name = activity_name;
    }

    @NonNull
    public String getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(@NonNull String activity_id) {
        this.activity_id = activity_id;
    }

    @NonNull
    @Override
    public String toString() {
        return activity_name;
    }
}
