package com.infodevelopers.cmisattendance.data.setup;

import com.infodevelopers.cmisattendance.data.local.model.VDC;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;


@Dao
public interface VDCDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insert(List<VDC> vdcs);

    @Update
    void update(VDC vdc);

    @Delete
    void delete(VDC vdc);

    @Query("SELECT * FROM VDC")
    Flowable<List<VDC>> getAll();

    @Query("SELECT * FROM VDC WHERE district_id IN (:districtId)")
    Flowable<List<VDC>> loadAllByIds(int districtId);

    /*@Query("SELECT * FROM user WHERE first_name LIKE :first AND " +
            "last_name LIKE :last LIMIT 1")
    VDC findByName(String first, String last);*/

}
