package com.infodevelopers.cmisattendance.data.setup;

import com.infodevelopers.cmisattendance.data.local.model.District;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import io.reactivex.Completable;
import io.reactivex.Flowable;

@Dao
public interface DistrictDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insert(List<District> districtList);

    @Update
    void update(District toleResponse);

    @Delete
    void delete(District toleResponse);

    @Query("SELECT * FROM district")
    Flowable<List<District>> getAll();

    @Query("SELECT * FROM district WHERE column_id IN (:toles)")
    Flowable<List<District>> loadAllByIds(int[] toles);
}
