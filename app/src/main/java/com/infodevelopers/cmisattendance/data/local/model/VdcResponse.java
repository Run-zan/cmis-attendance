package com.infodevelopers.cmisattendance.data.local.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VdcResponse {
    @SerializedName("data")
    private List<VDC> mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("status")
    private String mStatus;

    public List<VDC> getData() {
        return mData;
    }

    public void setData(List<VDC> data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }
}
