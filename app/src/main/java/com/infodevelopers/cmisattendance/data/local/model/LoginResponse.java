
package com.infodevelopers.cmisattendance.data.local.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class LoginResponse {

    @SerializedName("data")
    private Data mData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("status_code")
    private int mStatusCode;
    @SerializedName("token")
    private String mToken;

    public Data getData() {
        return mData;
    }

    public void setData(Data data) {
        mData = data;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public int getStatusCode() {
        return mStatusCode;
    }

    public void setStatusCode(int statusCode) {
        mStatusCode = statusCode;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        mToken = token;
    }

    public class Data {

        @SerializedName("CHECKIN_DETAILS")
        private List<Object> mCHECKINDETAILS;
        @SerializedName("LOGIN_NAME")
        private String mLOGINNAME;
        @SerializedName("NAME")
        private String mNAME;
        @SerializedName("USERID")
        private int mUSERID;

        public List<Object> getCHECKINDETAILS() {
            return mCHECKINDETAILS;
        }

        public void setCHECKINDETAILS(List<Object> cHECKINDETAILS) {
            mCHECKINDETAILS = cHECKINDETAILS;
        }

        public String getLOGINNAME() {
            return mLOGINNAME;
        }

        public void setLOGINNAME(String lOGINNAME) {
            mLOGINNAME = lOGINNAME;
        }

        public String getNAME() {
            return mNAME;
        }

        public void setNAME(String nAME) {
            mNAME = nAME;
        }

        public int getUSERID() {
            return mUSERID;
        }

        public void setUSERID(int uSERID) {
            mUSERID = uSERID;
        }

    }

}
