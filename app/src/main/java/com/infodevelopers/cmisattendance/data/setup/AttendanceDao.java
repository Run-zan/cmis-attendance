package com.infodevelopers.cmisattendance.data.setup;

import com.infodevelopers.cmisattendance.data.local.model.Attendance;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import io.reactivex.Completable;
import io.reactivex.Flowable;

@Dao
public interface AttendanceDao {

    @Insert
    Completable putAttendance(Attendance attendance);

    @Query("Select * from attendance")
    Flowable<List<Attendance>> getAllAttendance();

    @Query("Select * from attendance where attendance_id=:attendance_id")
    Flowable<Attendance> getAttendanceById(int attendance_id);




}
