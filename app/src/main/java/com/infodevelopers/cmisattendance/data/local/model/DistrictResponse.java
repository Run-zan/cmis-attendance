package com.infodevelopers.cmisattendance.data.local.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DistrictResponse {
    @SerializedName("data")
    private List<District> mData;

    @SerializedName("status")
    private String mStatus;

    public List<District> getData() {
        return mData;
    }

    public void setData(List<District> data) {
        mData = data;
    }


    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }


}
