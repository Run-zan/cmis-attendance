package com.infodevelopers.cmisattendance.data.di;

import com.infodevelopers.cmisattendance.data.LocalDataSource;
import com.infodevelopers.cmisattendance.data.RemoteDataSource;
import com.infodevelopers.cmisattendance.data.repository.DataRepository;
import com.infodevelopers.cmisattendance.di.scope.PerApp;

import javax.inject.Singleton;
import javax.sql.DataSource;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

@Provides
    LocalDataSource provideLocalData(LocalDataSource localDataSource){
    return localDataSource;
}

@Provides
    RemoteDataSource provideRemoteData(RemoteDataSource remoteDataSource){
    return remoteDataSource;
}
    @Provides
    @Singleton
    DataRepository getDataRepository(DataRepository dataRepository){
        return  dataRepository;
    };



}
