package com.infodevelopers.cmisattendance.data.repository;

import com.infodevelopers.cmisattendance.data.local.model.ActivityModel;
import com.infodevelopers.cmisattendance.data.local.model.Attendance;
import com.infodevelopers.cmisattendance.data.local.model.Child;
import com.infodevelopers.cmisattendance.data.local.model.ChildResponse;
import com.infodevelopers.cmisattendance.data.local.model.District;
import com.infodevelopers.cmisattendance.data.local.model.DownloadWrapper;

import com.infodevelopers.cmisattendance.data.local.model.ChildAttendance;
import com.infodevelopers.cmisattendance.data.local.model.VDC;
import com.infodevelopers.cmisattendance.data.local.model.Ward;
import com.infodevelopers.cmisattendance.module.login.pojo.LoginResponse;


import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;

public interface DataInterface {
     Observable<LoginResponse> loginUser(String username, String password);
//     ObservableSource<Boolean> putVdc(List<VDC> vdcs);
//     ObservableSource<Boolean> putWard(List<Ward> wardResponses);
//     ObservableSource<Boolean> putDistrict(List<District> toleResponses);
     Flowable<List<Ward>> getAllWard();
     Flowable<List<District>> getAllDistrict();
     Completable storeDownloadData(DownloadWrapper downloadWrapper);
     Flowable<List<VDC>> getAllVdc();
     ObservableSource<DownloadWrapper> getDownloadData();
     Flowable<List<VDC>> getVdcOfDistrict(int districtId);
     Flowable<List<Ward>> getWardOfVdc(int vdcId);
     Observable<ChildResponse> downloadChildData(int district,int vdc,int ward);
     Completable insertChildData(List<Child> childList);
     Flowable<List<Child>> getChildData(String district_id,String vdc_id,String ward_id);
     Flowable<List<ActivityModel>> getAllActivity();
     Flowable<List<Ward>> getDownloadedWardInfo();
     Completable insertAttendance(Attendance attendance);
     Flowable<List<Attendance>> getAllAttendance();
     Completable insertStudentAttendance(List<ChildAttendance> childAttendance);
     Flowable<List<ChildAttendance>> getStudentAttendance(int activity_id);


}
