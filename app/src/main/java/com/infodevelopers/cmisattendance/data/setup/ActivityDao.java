package com.infodevelopers.cmisattendance.data.setup;

import com.infodevelopers.cmisattendance.data.local.model.ActivityModel;

import java.util.List;

import androidx.constraintlayout.solver.widgets.Flow;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import io.reactivex.Completable;
import io.reactivex.Flowable;

@Dao
public interface ActivityDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insertActivity(List<ActivityModel> activityModels);

    @Query("SELECT * FROM ActivityModel")
    Flowable<List<ActivityModel>> getAllActivity();

    @Query("SELECT * FROM ActivityModel WHERE activity_id IN (:activity_id)")
    Flowable<ActivityModel> getActivityforId(String activity_id);

}
