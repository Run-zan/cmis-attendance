package com.infodevelopers.cmisattendance.di.module;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.infodevelopers.cmisattendance.base.UserSessionPrefs;

import com.infodevelopers.cmisattendance.utils.SchedulerProvider;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class ApplicationModule {
    private final Context app;

    @Inject
    public ApplicationModule(Context app) {
        this.app = app;
    }



    @Provides
    @Singleton
    Context Context() {
        return app;
    }

    @Provides
    @Singleton
    CompositeDisposable getCompositeDisposable(){
        return new CompositeDisposable();
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new Gson();
    }

    @Provides
    @Singleton
    SchedulerProvider provideSchedulerProvider() {
        return SchedulerProvider.getINSTANCE();
    }

    @Provides
    @Singleton
    SharedPreferences providesSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(app);
    }

    @Provides
    @Singleton
    UserSessionPrefs provideUserSession(Context context) {
        return new UserSessionPrefs(context);
    }


    /*@Provides
    @PerApp
    RxBus provideRxBus() {
        return RxBus.getInstance();
    }


    @Provides
    @PerApp
    ReactiveLocationProvider provideRxLocation(Context context) {
        return new ReactiveLocationProvider(context);
    }*/

}
