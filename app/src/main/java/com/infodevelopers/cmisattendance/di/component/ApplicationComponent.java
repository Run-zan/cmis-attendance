package com.infodevelopers.cmisattendance.di.component;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.infodevelopers.cmisattendance.base.UserSessionPrefs;
import com.infodevelopers.cmisattendance.data.di.DatabaseModule;
import com.infodevelopers.cmisattendance.data.di.RepositoryModule;
import com.infodevelopers.cmisattendance.data.setup.ActivityDao;
import com.infodevelopers.cmisattendance.data.setup.AttendanceDao;
import com.infodevelopers.cmisattendance.data.setup.ChildDao;
import com.infodevelopers.cmisattendance.data.setup.DistrictDao;
import com.infodevelopers.cmisattendance.data.setup.ChildAttendanceDao;
import com.infodevelopers.cmisattendance.data.setup.VDCDao;
import com.infodevelopers.cmisattendance.data.setup.WardDao;
import com.infodevelopers.cmisattendance.di.module.ApiModule;
import com.infodevelopers.cmisattendance.di.module.ApplicationModule;

import com.infodevelopers.cmisattendance.network.ApiInterface;
import com.infodevelopers.cmisattendance.utils.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Component;
import io.reactivex.disposables.CompositeDisposable;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        ApiModule.class,DatabaseModule.class,RepositoryModule.class
})

public interface ApplicationComponent {
    Context getContext();
    Gson getGson();
    SchedulerProvider getSchedulerProvider();
    ApiInterface getApiInterface();
    SharedPreferences getSharedPreferences();
    UserSessionPrefs getUserSessionProference();
    VDCDao getVdcDao();
    WardDao getWardDao();
    DistrictDao getDistrictDao();
    CompositeDisposable getComposite();
    ChildDao getChildDao();
    ChildAttendanceDao getStudentAttendanceDao();
    AttendanceDao getAttendanceDao();
    ActivityDao getActivityDao();






//    LoginComponent plus(LoginModule loginModule);
//    DownloadComponent plus(DownloadModule downloadModule);
}

