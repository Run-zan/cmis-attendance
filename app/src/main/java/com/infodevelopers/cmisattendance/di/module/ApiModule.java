package com.infodevelopers.cmisattendance.di.module;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.infodevelopers.cmisattendance.di.scope.PerApp;
import com.infodevelopers.cmisattendance.network.ApiInterface;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    Gson gson = new GsonBuilder().setLenient().create();

    @Provides
    @Singleton
    OkHttpClient provideOkhttpClient() {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        client.connectTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .addNetworkInterceptor(new StethoInterceptor());

        return client.build();

    }

    //Base Url
    String baseUrl = "http://182.93.90.34/cmis/api/index/";
    String baseUrlLogin = "http://192.168.40.25:82/opmisV2/api/v1/";

    @Provides
    @Singleton
    ApiInterface provideApiService(OkHttpClient okHttpClient){
        return new Retrofit.Builder()
                .baseUrl(baseUrlLogin)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build().create(ApiInterface.class);
        }

}

