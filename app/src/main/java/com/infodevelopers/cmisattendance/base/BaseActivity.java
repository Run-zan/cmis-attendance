package com.infodevelopers.cmisattendance.base;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.CallSuper;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Toast;


import com.afollestad.materialdialogs.MaterialDialog;
import com.infodevelopers.cmisattendance.R;
import com.infodevelopers.cmisattendance.base.view.BaseView;
import com.infodevelopers.cmisattendance.base.view.MvpView;
import com.infodevelopers.cmisattendance.utils.CommonUtils;

import butterknife.ButterKnife;
import butterknife.Unbinder;


public abstract class BaseActivity extends AppCompatActivity  implements BaseView,BaseFragment.Callback {

    private Unbinder mUnBinder;

    ProgressDialog progressDialog;
    MaterialDialog mDialog;


    @Override
    public void showLoading(String message) {
        if(mDialog==null) {
            mDialog = new MaterialDialog.Builder(this)
                    .cancelable(true)
                    .autoDismiss(false)
                    .content(message)
                    .progress(true, 0).build();

        }
        mDialog.show();

    }

    @Override
    public void hideLoading() {
        if(mDialog!=null && mDialog.isShowing()){
            mDialog.dismiss();
        }
    }

    @Override
    public void openActivityOnTokenExpire() {

    }

    @Override
    public void onError(int resId) {
     //   onError(R.string.app_name));
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public boolean isNetworkConnected() {
        return false;
    }

    @Override
    public void hideKeyboard() {

    }
    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());

        ButterKnife.bind(this);

        onViewReady(savedInstanceState, getIntent());
    }

    protected abstract int getContentView();


    public void setUnBinder(Unbinder unBinder) {
        mUnBinder = unBinder;
    }

    @CallSuper
    public void onViewReady(Bundle savedInstances, Intent intent){

    }




}

