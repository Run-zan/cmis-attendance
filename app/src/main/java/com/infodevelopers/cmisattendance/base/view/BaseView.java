package com.infodevelopers.cmisattendance.base.view;


import androidx.annotation.StringRes;

public interface BaseView {
    void initialize();
    void showLoading(String message);

    void hideLoading();

    void openActivityOnTokenExpire();

    void onError(@StringRes int resId);

    void onError(String message);

    void showMessage(String message);

    void showMessage(@StringRes int resId);

    boolean isNetworkConnected();

    void hideKeyboard();


}
