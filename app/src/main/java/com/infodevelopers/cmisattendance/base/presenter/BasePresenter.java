package com.infodevelopers.cmisattendance.base.presenter;

import com.infodevelopers.cmisattendance.base.view.BaseView;
import com.infodevelopers.cmisattendance.data.repository.DataRepository;
import com.infodevelopers.cmisattendance.utils.SchedulerProvider;
import javax.inject.Inject;
import io.reactivex.disposables.CompositeDisposable;
public class BasePresenter<V extends BaseView> implements MvpPresenter<V> {

    private static final String TAG = "BasePresenter";
    private final SchedulerProvider mSchedulerProvider;
    private final CompositeDisposable mCompositeDisposable;
    private final DataRepository mDataRepository;

    private V mMvpView;

    @Inject
    public BasePresenter(SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable, DataRepository dataRepository) {
        this.mSchedulerProvider = schedulerProvider;
        this.mCompositeDisposable = compositeDisposable;
        this.mDataRepository=dataRepository;
    }


    public DataRepository getDataRepository() {
        return mDataRepository;
    }

    @Override
    public void onAttach(V mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void onDetach() {
        mCompositeDisposable.clear();
        mMvpView = null;
    }

    @Override
    public void handleApiError(String error) {

    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    public V getMvpView() {
        return mMvpView;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) throw new MvpViewNotAttachedException();
    }



    public SchedulerProvider getSchedulerProvider() {
        return mSchedulerProvider;
    }

    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }




    @Override
    public void setUserAsLoggedOut() {
      //  getDataManager().setAccessToken(null);

    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }

    public static class MvpViewNotAttachedException extends RuntimeException {
        public MvpViewNotAttachedException() {
            super("Please call Presenter.onAttach(MvpView) before" +
                    " requesting data to the Presenter");
        }
    }
}
