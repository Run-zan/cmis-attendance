package com.infodevelopers.cmisattendance.base.presenter;

import com.infodevelopers.cmisattendance.base.view.BaseView;
import com.infodevelopers.cmisattendance.base.view.MvpView;

public interface MvpPresenter<V extends BaseView> {

    void onAttach(V mvpView);

    void onDetach();

    void handleApiError(String error);

    void setUserAsLoggedOut();

    void subscribe();
    void unSubscribe();
}
