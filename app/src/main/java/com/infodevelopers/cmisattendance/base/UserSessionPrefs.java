package com.infodevelopers.cmisattendance.base;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.infodevelopers.cmisattendance.module.login.pojo.LoginResponse;

public class UserSessionPrefs {

    SharedPreferences pref;
    SharedPreferences.Editor edit;
    Context con;


    public static final String PREFS_NAME = "CMIS_SHARED_PREFS";
    public static final String isLogin = "IS_LOGIN";



    public static final String schoolName = "SChool_name";

    public UserSessionPrefs(Context context) {
        this.con = context;
        pref = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        edit = pref.edit();
    }


    public void saveLogin(LoginResponse loginResponse) {

        edit.putBoolean(isLogin, true);
        pref.edit().putString(PREFS_NAME, new Gson().toJson(loginResponse)).commit();
        edit.commit();
    }

    public String getSchoolName(String schoolName) {
        SharedPreferences sharedPreferences = con.getSharedPreferences(schoolName, Context.MODE_MULTI_PROCESS);

        return sharedPreferences.getString(schoolName, "");
    }


    public void setSchoolName(String schoolName) {
        pref.edit().putString(schoolName, schoolName);
        edit.commit();
    }

    public String getsaveLogin() {
        SharedPreferences sharedPreferences = con.getSharedPreferences(PREFS_NAME, Context.MODE_MULTI_PROCESS);
        return sharedPreferences.getString(PREFS_NAME, "");
    }

    public Boolean isUserLogin() {
        SharedPreferences sharedPreferences = con.getSharedPreferences(PREFS_NAME, Context.MODE_MULTI_PROCESS);
        return sharedPreferences.getBoolean(isLogin, false);
    }

    public void isUserLogOut() {
        SharedPreferences sharedPreferences = con.getSharedPreferences(PREFS_NAME, Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.clear();
        edit.commit();
    }
}
